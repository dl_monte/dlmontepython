{
 "cells": [
  {
   "cell_type": "markdown",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Python utilities for DL-MONTE\n",
    "\n",
    "Authors: Kevin Stratford, Tom Underwood\n",
    "\n",
    "## Introduction\n",
    "\n",
    "Normally solving a given problem using molecular simulation is more complex than simply performing\n",
    "a single simulation and analysing its output. Typically workflows must be employed\n",
    "which involve cycles of running one or more simulations, analysing their output, \n",
    "and then using the results of this analysis to inform input parameters for further simulations.\n",
    "A 'toolkit' of helper utilities for performing such tasks is thus desirable. \n",
    "With this in mind, we have developed a Python toolkit to support DL_MONTE (with the intention of\n",
    "eventually extending its scope beyond DL_MONTE). \n",
    "\n",
    "This tutorial describes some utilities which help to read, manipulate, and write the inputs \n",
    "and outputs associated with DL_MONTE. Moreover it describes how to execute DL_MONTE from Python.\n",
    "The toolkit is named `dlmontepython`, however it was formally called `htk` (Histogram reweighting toolkit - 'histogram reweighting' being one of\n",
    "the key functionalities of the toolkit). To elaborate, this tutorial covers:\n",
    "\n",
    "1. Reading inputs (`CONFIG`, `CONTROL` and `FIELD` files)\n",
    "2. Reading output files into python (`PTFILE` and `YAMLDATA` files)\n",
    "3. Running the DL-MONTE executable from python\n"
   ]
  },
  {
   "cell_type": "markdown",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "\n",
    "## Set-up\n",
    "\n",
    "### Python version\n",
    "\n",
    "Please note this notebook uses `print()` and so requires Python 3 (usually displayed at the top right of the notebook).\n",
    "You can also check the python version with:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import sys\n",
    "print(sys.version_info)"
   ]
  },
  {
   "cell_type": "markdown",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "### Adding the toolkit to `PYTHONPATH`\n",
    "`PYTHONPATH` is the shell variable which contains a list of directories in which to look for packages to be imported. To use the toolkit, it is prudent to set `PYTHONPATH` to the directory containing the toolkit.\n",
    "\n",
    "One can modify `PYTHONPATH` to include the directory containing the toolkit (which will depend on your local system!) within Python as follows:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Import standard os module for file manipulations\n",
    "import os\n",
    "\n",
    "# You will need to set DL_MONTE_HOME appropriately for\n",
    "# the local system\n",
    "MONTEPYTHON_HOME = \"/home/jap93/DLMONTE3/dlmontepython_course/\"\n",
    "\n",
    "# Set PYTHONPATH to the 'htk' directory within DL_MONTE_HOME\n",
    "sys.path.append(MONTEPYTHON_HOME)"
   ]
  },
  {
   "cell_type": "markdown",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "Alternatively one can modify `PYTHONPATH` directly in the shell via:\n",
    "\n",
    "```\n",
    "bash> cd $DL_MONTE_HOME/htk\n",
    "bash> export PYTHONPATH=$PYTHONPATH:`pwd`\n",
    "```\n",
    "\n",
    "where here `$DL_MONTE_HOME` is the main directory containing DL_MONTE.\n",
    "This is the prefered method because it does not rely on PYTHONPATH being known\n",
    "by the script importing the toolkit."
   ]
  },
  {
   "cell_type": "markdown",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "Within Python the toolkit is imported as follows:"
   ]
  },
  {
   "cell_type": "markdown",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "### Importing the `dlmonte` module\n",
    "\n",
    "The functionality we will examine here is contained in the `dlmonte` module of the toolkit. This is imported as follows:\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Import the dlmonte module with alias \"dlmonte\"\n",
    "import htk.sources.dlmonte as dlmonte"
   ]
  },
  {
   "cell_type": "markdown",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "### Setting the directory contining tutorial input files\n",
    "\n",
    "In this tutorial we will use some pre-existing DL_MONTE input files in order to demonstrate the functionality of the `dlmonte` module. These files are distributed with the tutorial, and pertain to a short grand-canonical Monte Carlo simulation of a Lennard-Jones fluid near the critical point. The directory containing these files is `util-dlmonte_files`. We will store this directory in the variable `input_dir`, which may need to be set appropriately for your local system:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# You may need to set input_dir appropriately for the local system\n",
    "input_dir = \"util-dlmonte_files\""
   ]
  },
  {
   "cell_type": "markdown",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "## Reading and writing input files \n",
    "\n",
    "We will now examine the `dlmonte` module's functionality regarding importing and exporting the three key DL_MONTE input files:\n",
    "\n",
    "1. `FIELD`\n",
    "2. `CONFIG`\n",
    "3. `CONTROL`\n",
    "\n",
    "The input files we will use as an example are contained in `input_dir`"
   ]
  },
  {
   "cell_type": "markdown",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "### Reading a `FIELD` file\n",
    "\n",
    "The `dlmonte.dlfield` module provides a method `from_file()` which loads\n",
    "a `FIELD` file into an internal python structure (an instance of class `FIELD`).\n",
    "\n",
    "The method takes the path of the `FIELD` file as the argument. \n",
    "\n",
    "In this case, as mentioned above, the input file corresponds to a small grand canonical simulation using Lennard-Jones particles (taken from the DL_MONTE test suite).\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "filename = os.path.join(input_dir, \"FIELD\")\n",
    "\n",
    "field = dlmonte.dlfield.from_file(filename)"
   ]
  },
  {
   "cell_type": "markdown",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#### The python FIELD class and its attributes\n",
    "\n",
    "We can now examine the contents of the `dlfield`\n",
    "structure.\n",
    "\n",
    "Using the builtin `repr()` method shows the internal representation of the whole `FIELD` structure:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "repr(field)"
   ]
  },
  {
   "cell_type": "markdown",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "\n",
    "The `description`, `cutoff`, and `units` attributes are of type string, integer, and string, respectively.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(\"Description: \", field.description)\n",
    "print(\"Cutoff:      \", field.cutoff)\n",
    "print(\"Units:       \", field.units)"
   ]
  },
  {
   "cell_type": "markdown",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "\n",
    "Other attributes, such as `atomtypes` are more complex; `atomtypes` is a list of entries of class `AtomType`. In this example, there is only one atom type present in the simulation:\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print (\"Atomtypes: \", field.atomtypes)"
   ]
  },
  {
   "cell_type": "markdown",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "\n",
    "\n",
    "Non-bonded interations are stored in the `vdw` attribute (again a list), which provides a full description of the interaction:\n",
    "\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print (repr(field.vdw[0].atom1))\n",
    "print (repr(field.vdw[0].atom2))\n",
    "print (repr(field.vdw[0].interaction))"
   ]
  },
  {
   "cell_type": "markdown",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "Direct access to numerical values for computation is available, e.g.,\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ljinteraction = field.vdw[0].interaction\n",
    "\n",
    "print (\"Twice epsilon is \", 2.0*ljinteraction.epsilon)"
   ]
  },
  {
   "cell_type": "markdown",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "### Writing `FIELD` file output\n",
    "\n",
    "The `dlfield` module also allows you to write a FIELD file in a well-formed format for use by the main DL-MONTE executable.\n",
    "\n",
    "The is done via the `str()` method of the FIELD object (i.e., the output of `print` or the output of a format statement`\"{!s}\".format(...)`:\n",
    "\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(field)"
   ]
  },
  {
   "cell_type": "markdown",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "\n",
    "This allows us, if required, to manipulate and write a new `FIELD` file with\n",
    "updated parameters. For example, to adjust the potential interaction,\n",
    "we could write:\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "field.vdw[0].interaction.epsilon = 2.0\n",
    "\n",
    "print(field)"
   ]
  },
  {
   "cell_type": "markdown",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "Note that the 'epsilon' parameter for the Van der Waals Lennard-Jones interaction has been modified.\n",
    "\n",
    "The internal python representation is flexible enough that additional output formats are constructed relatively easily. For example, JSON output is available via:\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(field.to_json())"
   ]
  },
  {
   "cell_type": "markdown",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "\n",
    "### Reading and writing a `CONFIG` file\n",
    "\n",
    "Likewise, a `dlmonte.dlconfig.from_file()` method is available which reads the contents of the `CONFIG` file into an internal represetation.\n",
    "\n",
    "As this is related to the `FIELD` description, the `FIELD` object can, optionally, be passed as an argument. If the `FIELD` reference is provided, the two files can be checked for consistency.\n",
    "\n",
    "However, the `CONFIG` file can be read independently. E.g.,\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# The CONFIG will be from `input_dir`\n",
    "\n",
    "filename = os.path.join(input_dir, \"CONFIG\")\n",
    "config = dlmonte.dlconfig.from_file(filename)"
   ]
  },
  {
   "cell_type": "markdown",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "The internal representation of the `CONFIG` object is:\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "repr(config)"
   ]
  },
  {
   "cell_type": "markdown",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "Again, the `config` structure has a number of attributes. Of particular interest (especially for NVT and muVT ensembles) is the `vcell` attribute which specifies, indirectly, the volume of the system.\n",
    "\n",
    "A utility method if provided to return the volume of the cell.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print (\"Cell vectors: \", config.vcell)\n",
    "print (\"Cell volume:  \", config.volume())"
   ]
  },
  {
   "cell_type": "markdown",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "\n",
    "Note that the internal representation produced via `repr()` does not show\n",
    "all the molecule information, as this may be very long.\n",
    "\n",
    "However, the string formating option can be used to generate output which is a well-formed `CONFIG` file with full information."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print (config)"
   ]
  },
  {
   "cell_type": "markdown",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "\n",
    "### Reading and writing a `CONTROL` file\n",
    "\n",
    "\n",
    "\n",
    "\n",
    "\n",
    "Simulation execution and additional parameters are determined by the `CONTROL` file."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Again using the input_dir location as defined above\n",
    "\n",
    "filename = os.path.join(input_dir, \"CONTROL\")\n",
    "ctrl = dlmonte.dlcontrol.from_file(filename)"
   ]
  },
  {
   "cell_type": "markdown",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "\n",
    "The `CONTROL` file has a potentially complex structure which is split into three basic parts: a title, a 'use' block, and a 'main' block. The first is just the title string:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "repr(ctrl.title)"
   ]
  },
  {
   "cell_type": "markdown",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "The second part is the use block, which contains relevant 'use' statements, and any FED block (see the manual for details). In this case, these are both empty:\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "repr(ctrl.use_block)"
   ]
  },
  {
   "cell_type": "markdown",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "The third part is the main section, which contains a series of statements controlling various simulation behaviour:\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "repr(ctrl.main_block)"
   ]
  },
  {
   "cell_type": "markdown",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "Again, the string output is designed to provide a well-form `CONTROL` file suitable for use with the DL_MONTE executable:\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print (ctrl)"
   ]
  },
  {
   "cell_type": "markdown",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "\n",
    "### Manipulating `FIELD`, `CONFIG` and `CONTROL`  in one go\n",
    "\n",
    "It is convenient to read and store the three mandatory input files in a single step. A container class `DLMonteInput` is provided to do this:\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Read all input files in input_dir into a DLMonteInput object\n",
    "inputs = dlmonte.DLMonteInput.from_directory(input_dir)"
   ]
  },
  {
   "cell_type": "markdown",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "\n",
    "This reads the three files with standard filenames `FIELD`, `CONFIG` and `CONTROL` from the named directory.\n",
    "\n",
    "The result contains `field`, `config`, and `control` objects as attributes.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print (repr(inputs.field))\n",
    "print ()\n",
    "print (repr(inputs.config))\n",
    "print ()\n",
    "print (repr(inputs.control))"
   ]
  },
  {
   "cell_type": "markdown",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "<br>\n",
    "<br>\n",
    "<br>"
   ]
  },
  {
   "cell_type": "markdown",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "\n",
    "\n",
    "## Running a DL_MONTE  simulation from python\n",
    "\n",
    "We now have python representations of the `FIELD`, `CONFIG`, and `CONTROL` files required for a DL_MONTE simulation.\n",
    "\n",
    "This section discusses running DL_MONTE via python.\n"
   ]
  },
  {
   "cell_type": "markdown",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#### STEP 1: Locate exectable and input files, and amend input files"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# We need an executable. \n",
    "# We assume here it is in the DL_MONTE_HOME directory (see above), and is from a serial compilation\n",
    "DL_MONTE_HOME = \"/home/jap93/DLMONTE3/DL_MONTE-2/bin\"\n",
    "dlx = os.path.join(DL_MONTE_HOME, \"DLMONTE-SRL.X\")\n",
    "\n",
    "# For input again use that from input_dir defined above.\n",
    "\n",
    "myinput = dlmonte.DLMonteInput.from_directory(input_dir)\n",
    "\n",
    "\n",
    "# We also amend the CONTROL file here...\n",
    "# Update the main block of the CONTROL file to include YAML output directive (see manual)\n",
    "\n",
    "yamltag={\"yamldata\": 1000}\n",
    "myinput.control.main_block.statements.update(yamltag)"
   ]
  },
  {
   "cell_type": "markdown",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#### STEP 2: Copy the input to a working directory\n",
    "\n",
    "We assume we need to copy the input, without manipulation, to a working directory where the run will take place (and where output will be produced).\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Set an appropriate working directory\n",
    "# THIS MUST BE CREATED ON YOUR LOCAL SYSTEM: E.G. mkdir util-dlmonte_workspace\n",
    "\n",
    "work_dir = \"/home/jap93/util-dlmonte_workspace\"\n",
    "\n",
    "# Copy the input to the working directory\n",
    "\n",
    "myinput.to_directory(work_dir)"
   ]
  },
  {
   "cell_type": "markdown",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#### STEP 3: Create a `DLMonteRunner` object, and execute DL_MONTE\n",
    "\n",
    "This is a utility to help run the DL MONTE executable in the working directory via a sub-process.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Set up a DLMonteRunner object linked to the directory work_dir\n",
    "# and the executable dlx\n",
    "myrun = dlmonte.DLMonteRunner(dlx, work_dir)\n",
    "\n",
    "# Execute the runner - the output files from the simulation will be in work_dir\n",
    "myrun.execute()"
   ]
  },
  {
   "cell_type": "markdown",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#### STEP 4: Examine results\n",
    "\n",
    "On successful execution, the `DLMonteRunner` creates a `DLMonteOutput` object which contains any `PTFILE` and/or `YAMLDATA` output.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# We will look at the YAML-format data output by the simulation, which is stored in the\n",
    "# output file YAMLDATA, and within the DLMonteRunner object as follows:\n",
    "data = myrun.output.yamldata.data\n",
    "\n",
    "# Print each frame of YAML-format data, where each frame corresponds to a certain timestep \n",
    "# ('timestamp') in the simulation. Note that data was only output every 100 moves\n",
    "for step in data:\n",
    "    print (step)"
   ]
  },
  {
   "cell_type": "markdown",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#### STEP 5: Clean up\n",
    "\n",
    "To remove input, output, or both, methods on the `DLMonteRunner` class are provided:\n",
    "\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Remove input or output\n",
    "myrun.remove_input()\n",
    "myrun.remove_output()\n",
    "\n",
    "# Or, remove both input and output\n",
    "myrun.cleanup()\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "anaconda-cloud": {},
  "kernelspec": {
   "display_name": "Python 3.7.4 64-bit",
   "language": "python",
   "name": "python37464bit5f73c22a59314460943a8a7e2d2be13a"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 2
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython2",
   "version": "3.7.4-final"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 0
}