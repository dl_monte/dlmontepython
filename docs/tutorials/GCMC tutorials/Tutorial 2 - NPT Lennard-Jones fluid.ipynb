{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "623d5712",
   "metadata": {},
   "source": [
    "TUTORIAL 2 : NPT Lennard-Jones fluid\n",
    "====================================\n",
    "\n",
    "Original authors: Andrey V. Brukhno (andrey.brukhno{at}stfc.ac.uk), James Grant (r.j.grant{at}bath.ac.uk), and John Purton (john.purton{at}stfc.ac.uk)\n",
    "\n",
    "Modified to use dlmontepython by Joe Manning (joseph.manning{at}manchester.ac.uk)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "c5055045",
   "metadata": {},
   "source": [
    "Introduction\n",
    "------------\n",
    "\n",
    "In the NpT ensemble number of particles, pressure and temperature (*N,p,T*) are kept constant, which implies that apart from the particle moves the volume is also allowed to vary.\n",
    "That is, an additional MC move, uniformly sampling the volume, is attempted with the acceptance probability:\n",
    "\n",
    "$$ P_{acc}([r_1,V_1] \\rightarrow [r_2,V_2]) = \\min(1, \\exp \\{- \\beta [U(r_2) - U(r_1) + p_{ext}(V_{2}-V_{1}) - N \\beta^{-1} \\ln(V_{2} / V_{1}) ] \\} ) $$\n",
    "where $p_{ext}$ is the external pressure. The particle coordinates are assumed to be scaled accordingly.\n",
    "\n",
    "NOTE: The last term under the exponent needs to be modified in the cases where, for example, linear dimensions of the (cubic) cell or $\\ln(V)$ are directly sampled instead of volume (keywords **linear** and **log** respectively).\n",
    "\n",
    "In order to proceed to NpT simulation, we will modify a copy of CONTROL file that we used in Tutorial 1 for NVT simulation, wherein we need to introduce the directives specifying the volume move and the accumulation of additional statistics. "
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b4513302",
   "metadata": {},
   "source": [
    "### Setting up a CONTROL file with dlmontepython\n",
    "Last time we walked through building a control file step by step. This time we'll make it a bit faster! Note, we'll use identical input files from the previous tutorial, so feel free to look back and copy/paste or rebuild them from Tutorial 1.\n",
    "\n",
    "The control file: \n",
    "```\n",
    "   NPT simulation of Lennard-Jones fluid\n",
    "   use ortho\n",
    "   finish\n",
    "   seeds 12 34 56 78               # Seed RNG seeds explicitly to the default\n",
    "   nbrlist auto                    # Use a neighbour list to speed up \n",
    "                                   # energy calculations\n",
    "   maxnonbondnbrs 512              # Maximum number of neighbours in neighbour list\n",
    "   temperature     1.4283461511745 # Corresponds to T*=1.1876; \n",
    "                                   # T(in K) = T* / BOLTZMAN \n",
    "   pressure        0.0179123655568\n",
    "   steps          110000           # Number of moves to perform in simulation\n",
    "   equilibration   10000           # Equilibration period: statistics \n",
    "                                   #are gathered after this period\n",
    "   print           10000           # Print statistics every 'print' moves\n",
    "   stack           10000           # Size of blocks for block averaging to obtain statistics\n",
    "   sample coord    10000           # How often to print configurations to ARCHIVE.000\n",
    "   sample volume 1 1.0             # sample volume every V-step, with the bin size of 1 A^3\n",
    "   yamldata        1000            # collect YAML stats every 1000 move\n",
    "\n",
    "   revconformat  dlmonte           # REVCON file is in DL_MONTE CONFIG format\n",
    "   archiveformat dlpoly4           # ARCHIVE.000/HISTORY.000/TRAJECTORY.000 format \n",
    "                                   # In this case: HISTORY.000 in DLPOLY4 style\n",
    "   move atom 1 512                 # Move atoms with a weight of 512\n",
    "   LJ core \n",
    "   move volume cubic linear 1      # Move volume, box is cubic, \n",
    "                                   # linear scaling with a weight of 1\n",
    "   start\n",
    "```\n",
    "\n",
    "The directive to invoke volume moves is **move volume**. The parameters to specify in the present tutorial are **cubic**, **linear** and **1** for the weight of volume moves among other MC steps (see the manual for further details). Note that the weight of atom moves has been set to 512 (see **move atom** directive).\n",
    "Volume moves are more computationally intensive than single atom moves, so the *rule of thumb* is to attempt one volume move each time every atom has been attempted to move (so called \"pass\" or \"sweep\" through the system)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "id": "f1d15a26",
   "metadata": {
    "ExecuteTime": {
     "end_time": "2022-07-28T18:15:41.185005Z",
     "start_time": "2022-07-28T18:15:41.175006Z"
    }
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "NPT example\n",
      "use ortho \n",
      "finish use-block\n",
      "seeds 12 34 56 78\n",
      "nbrlist auto\n",
      "maxnonbondnbrs 512\n",
      "temperature 1.4283461511745\n",
      "pressure 0.0179123655568\n",
      "steps 110000\n",
      "equilibration 10000\n",
      "print 10000\n",
      "stack 10000\n",
      "yamldata 1000\n",
      "revconformat dlmonte\n",
      "archiveformat dlpoly4\n",
      "sample coords 10000\n",
      "sample volume 1 1\n",
      "move atom 1 512\n",
      "LJ core\n",
      "move volume cubic linear 1\n",
      "start simulation\n"
     ]
    }
   ],
   "source": [
    "from dlmontepython.htk.sources import dlcontrol, dlmove\n",
    "\n",
    "\n",
    "useblock = dlcontrol.UseBlock(use_statements={'ortho': None)\n",
    "\n",
    "moves = [\n",
    "    dlmove.AtomMove(pfreq=512, movers = [{\"id\": 'LJ core'}]),\n",
    "    dlmove.VolumeCubicMove(pfreq=1, sampling='linear')\n",
    "]\n",
    "\n",
    "samples = {\n",
    "    'coords': {'nfreq': 10000},\n",
    "    'volume': {'binsize': 1, 'nfreq': 1}\n",
    "    \n",
    "}\n",
    "\n",
    "mainblock = dlcontrol.MainBlock(\n",
    "    statements={\n",
    "        'seeds': {'seed0': 12, 'seed1': 34, 'seed2': 56, 'seed3': 78},\n",
    "        'nbrlist': 'auto',\n",
    "        'maxnonbondnbrs': 512,\n",
    "        'temperature': 1.4283461511745,\n",
    "        'pressure': 0.0179123655568,\n",
    "        'steps': 110000,\n",
    "        'equilibration': 10000,\n",
    "        'print': 10000,\n",
    "        'stack': 10000,\n",
    "        'yamldata': 1000,\n",
    "        'revconformat': 'dlmonte',\n",
    "        'archiveformat': 'dlpoly4'\n",
    "    },\n",
    "    samples=samples,\n",
    "    moves=moves\n",
    ")\n",
    "\n",
    "control = dlcontrol.CONTROL(\n",
    "    title='NPT example',\n",
    "    use_block=useblock,\n",
    "    main_block=mainblock\n",
    ")\n",
    "print(control)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "id": "0e64b7d3",
   "metadata": {
    "ExecuteTime": {
     "end_time": "2022-07-28T18:23:57.918930Z",
     "start_time": "2022-07-28T18:23:57.909930Z"
    }
   },
   "outputs": [],
   "source": [
    "import os\n",
    "try:\n",
    "    os.mkdir('./tutorial_2_simulation')\n",
    "except FileExistsError:\n",
    "    pass\n",
    "\n",
    "with open('./tutorial_2_simulation/CONTROL', 'w') as f:\n",
    "    f.write(str(control))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b85d13a6",
   "metadata": {},
   "source": [
    "Exercise\n",
    "--------\n",
    "\n",
    "Run the simulation as before, then we'll import the volume data from the YAMLDATA file"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 32,
   "id": "bc2aab41",
   "metadata": {
    "ExecuteTime": {
     "end_time": "2022-07-28T19:00:25.118455Z",
     "start_time": "2022-07-28T19:00:24.952421Z"
    }
   },
   "outputs": [
    {
     "data": {
      "text/plain": [
       "<AxesSubplot:xlabel='timestamp'>"
      ]
     },
     "execution_count": 32,
     "metadata": {},
     "output_type": "execute_result"
    },
    {
     "data": {
      "image/png": "iVBORw0KGgoAAAANSUhEUgAAAX0AAAEGCAYAAACJnEVTAAAAOXRFWHRTb2Z0d2FyZQBNYXRwbG90bGliIHZlcnNpb24zLjUuMSwgaHR0cHM6Ly9tYXRwbG90bGliLm9yZy/YYfK9AAAACXBIWXMAAAsTAAALEwEAmpwYAAAzfklEQVR4nO3deXzcVbn48c8zk2SyL82+tUm6t+lG01IolFKWVkAWBS2K4gK9IPi7Xq8IqFdwuyoIIiooCla8yiIgm6AUKBToDnSnTfdm39qk2ZeZ8/tjvhPSdtJsk0lm5nm/XvPqzPlu56TtkzNnFWMMSimlQoNtpDOglFLKfzToK6VUCNGgr5RSIUSDvlJKhRAN+kopFULCRjoDfUlJSTF5eXkjnQ2llAoo77//fq0xJvXk9FEf9PPy8ti8efNIZ0MppQKKiBz2lt5n846IPCYi1SKy46T0r4vIHhHZKSL3WGl5ItIqIlus1+96nD9XRLaLyD4ReVBEZKiFUkopNTD9qemvBH4DPO5JEJHzgSuAmcaYdhFJ63H+fmPMbC/3eRhYAawHXgGWAa8OLttKKaUGo8+avjFmDXD0pOSbgZ8ZY9qtc6pPdw8RyQTijTHrjHsK8OPAlYPKsVJKqUEbbJv+JOBcEfkJ0AZ8yxizyTqWLyIfAseB7xlj3gGygdIe15daaV6JyArc3woYO3bsILOolAoGnZ2dlJaW0tbWNtJZGZUiIyPJyckhPDy8X+cPNuiHAUnAAmAe8LSIFAAVwFhjTJ2IzAWeF5HpgLf2+14X/THGPAI8AlBUVKSLAykVwkpLS4mLiyMvLw/tCjyRMYa6ujpKS0vJz8/v1zWDHadfCjxn3DYCLiDFGNNujKmzMvM+sB/3t4JSIKfH9TlA+SCfrZQKIW1tbSQnJ2vA90JESE5OHtC3oMEG/eeBJdZDJwERQK2IpIqI3UovACYCB4wxFUCjiCywRu18EXhhkM9WSoUYDfi9G+jPps/mHRF5AlgMpIhIKXAX8BjwmDWMswO43hhjRGQR8EMR6QKcwE3GGE8n8M24RwJF4R61oyN31KhT3djGExtKcLpcAOSnxnD5rGzsNg06Kjj0GfSNMdf2cug6L+c+Czzby302A4UDyp1SfvaXdYf59Zv7EAHPVhN/eu8QP7yikNm5iSOaNzX6HTp0iMsuu4wdO3b0ffII0bV3lOph/YE6ZuUkcPCnl3Lwp5fwq+WzqWxo46qH3uMv6w6NdPaUGjIN+kpZ2jqdbC1pYEFBMuBuK71idjZv/Pd5FGYl8PTm0j7uoILR7bffzkMPPdT9+e677+a+++7jtttuo7CwkBkzZvDUU0+dct3KlSu59dZbuz9fdtllvPXWWwDExsZy++23M3fuXC688EI2btzI4sWLKSgo4MUXXwTA6XRy2223MW/ePGbOnMnvf/97n5Rn1K+9o5S/fHDkGB1OF2cWjDkhPS4ynHMnpvDImgO0dTqJDLePUA7VD17aya7y4z6957SseO765PRejy9fvpxvfOMbfO1rXwPg6aef5vbbb+df//oXW7dupba2lnnz5rFo0aJ+P7O5uZnFixfz85//nKuuuorvfe97rFq1il27dnH99ddz+eWX8+ijj5KQkMCmTZtob29n4cKFXHzxxf0emtkbDfpKWdYfOIpNoChvzCnHZuYk0uUyfFRxnDljk0Ygd2qkzJkzh+rqasrLy6mpqSEpKYktW7Zw7bXXYrfbSU9P57zzzmPTpk3MnDmzX/eMiIhg2bJlAMyYMQOHw0F4eDgzZszg0KFDALz22mts27aNZ555BoCGhgb27t2rQV8pX9lwoI7pWQnER546s9HTibu1pF6D/gg6XY18OF199dU888wzVFZWsnz5cvbv39/nNWFhYbisUWDACWPpw8PDu4da2mw2HA5H9/uuri7APfHq17/+NUuXLvVlUbRNXylwt+d/WFLPmfmn1vIBMhIiSYtzsLW0wc85U6PB8uXLefLJJ3nmmWe4+uqrWbRoEU899RROp5OamhrWrFnD/PnzT7gmLy+PLVu24HK5KCkpYePGjQN65tKlS3n44Yfp7OwEoLi4mObm5iGXRWv6SgFbSurp6HJxptWJ682s3ES2ltb7L1Nq1Jg+fTqNjY1kZ2eTmZnJVVddxbp165g1axYiwj333ENGRkZ30wzAwoULyc/PZ8aMGRQWFnLGGWcM6Jk33HADhw4d4owzzsAYQ2pqKs8///yQyyLGjO6lbYqKioxuohJYDtQ0kRzjICG6fwtAjQa/en0vD7xRzJb/ubjXfP929T7u/fcett51MQlRgVO2QPfRRx8xderUkc7GqObtZyQi7xtjik4+V5t3lE/Vt3TwyV+/y+f+uJ4up6vvC0aIy2W4+8WdPPbuQTq6XGw4WMfUjPjT/qKamZMAwHZt4lEBTIN+iCmvb6W1w9nr8eKqRtbtrxv0/f+y7jDNHU52lh/nD+8cHPR9htv7R46xcu0hfvjyLi68/23eP3zslKGaJ5uZnQigTTwqoGnQDyDrD9Sx+dDJ+9n03wtbyjj3ntUU/XgV33x6C+/sraFn896mQ0e56rfv8fk/rueNj6oGfP/WDid/WnuIJVPSWDo9nQdeL+ZATdOg8zucnv+wjKhwO7+7bi7REXbau1ycOzHltNckRIeTnxLD1pJ6/2RSdRvtzdAjaaA/G+3IDQBOl+H+VXv47Wr3MLGvnpPPbUsnD2iS0NObSrj9uW0UjUsiPyWGV3dU8twHZczKSeD2T0zBJsJXVm4iIyGS6Ag7t/7tQ55csYBZA1hv5unNJRxt7uCm88aTlxzNBfe/zR3PbefJGxdgG0ULlnV0ufjn9goumpbOssIMLpqWzp7KRqZmxvV57aycBNYdGPw3ITVwkZGR1NXV6fLKXnjW04+MjOz3NRr0R7maxnb+88kPWbu/jmvn5xJut/Houwd5d28tywozAIgMt/PZebmMiYnweo8/rz3EXS/udM8q/UIRURF2fnhFIS9uLeeBVcV87g8bsNuEgpQY/nrjmQB86qG1fPXPm3ju5oWMTY7uM5+dThePrDnA3HFJzMtLQkT4n0un8e1nt/H8ljI+dUZOn/fwlzXFNdS3dHLlnCwA7DZhWlZ8v66dlZvI81vKqWxoIyOh///R1ODl5ORQWlpKTU3NSGdlVPLsnNVfGvRHgDGGneXHeW1XFe/urWF+fjL/ddFEHGEf19y7nC7+sv4w968qpqPLxb1Xz+SaolwAzp+Sxnee286v3tjbff5fNxzmj9cXMSXjxOD1x3cO8ON/fsRF09L5zefmdD8jMtzOZ4pyuXxWFn9Zd5gPjhzjR1cWkhLrniSy8svz+fTDa7n01+/w3Uum8tl5ub3Wspwuw1/XH6asvpW7L5/efd41RTn88d0DPPruQa6akz1qamn/2FLGmJgIzp2YOuBrZ+YkAu4hnssSMnycM+VNeHj4kGehqo8F7ZDNNcU1OI0hISqcOEcY1Y3tHKxtpqKhlahwO/FR4aTFRbKgYAyJ0d5ryP3lchle2FqG3WZj6fT0E4J3T/uqm3hhSxnPbymj5GgrNoFJ6XHsrmxkelY8v1o+GxFhTXENT20qYXdlI+dOTOGuT05nQlpsr8/fWlLPjY9vpqm9i599eibnT04lLjKch97axz3/2sOlMzJ5YPlswu0D68I5UNPEnc9tZ8PBo8zPH8PC8ae2eZfXt/LG7ipqmzoozI7nxVvOOaEp568bDvPdf+zg7zedxTwvyxsMlwM1Tdz3WjGd1giisWOi+fqSidhsUPTj1/lMUS4/unLgK323dTqZ9+PXOWdiCg9fN9fX2VbKZ3obshm0Qf+C+95if82ps9dsAi5z4udZuYnMzx9DQUoM45JjqG/pYFtpA8VVTSyalMJn5+X2Gsib27v41t+38uqOSgCSosO5ck422YlRALR0ONlVfpztZQ2U1bsD/cIJKXxyZhYXTE0jOdbBazsruf3ZbdS3dnav4T4hLZZvXTyZpdPT+1VDrjrexorHN3fPGB0TE8HR5g6umJ3FfdfMImyAAd/D5TI8vbmEn/1rN/Utnaccj3WEsXhyKkunZ7BkShoxjhO/PLZ0dLHgf9/g3Imp/PbzA5ucMhTffGoLL2+voCAlBoC91U0kRUdw/uRU/v5+Kc/efBZzxw3ul9D9r+3hwTf38cr/O7e7WeiZ90v50cu7uoepTs9K4JfLZ3f/O1DK30Iu6B+sbeZocwfHWzs53tZJaqyDvJQYMuIj6XS5aGzr4nBdM2uKa3m7uIad5Q10Oj/+WYTZhIyESEqPtZKZEMl/LCrgzIJk8lNiiAy3c6y5g73VTXz/hR0UVzXynUumMjkjjic3lvDarsoT7jUuOZoZ2QnMHZfEpTMySYs/tS24+ngbj753kNykaM6blErumL7b0U/W1unkrT01HKpr5nBdMxnxUdy6ZIJPdn0yxuDtn4pI39u1/e8rH/Houwd559vnk+WHIFjb1M7ZP32Ta+fn8oMr3LX5HWUN3PHcNnaUHSd3TBRrbjt/0M1NDS2dnHPPm5w9Ppnff6GIfdWNXPrgu0zJjGfeuCS6XIZn3y8lPMzGbz43h7O9fENSariFXNAfKKfLUF7fysHaZuIiw5iaGY8jzMba/XXcv6qY9w8fA9zfDGIiwmhsdy+KlBAVzm8/dwbn9Bju19bppMOq8YXbbERFhPZSvCVHWzjv3tXcdN54vr1syrA/zzNz9vVvnndCs1iX08XTm0vJS47m7AlDC8S/XFXMr97Yy/O3LOR7z2+n7Fgr//7Gou5f6Ptrmljx+GYO1bXwwGdn88lZWUN6nlIDpUF/CIwxFFc1saeqkX3VTdS3dDB2TDR5yTHMGZtIstX5qXq34vHNbDp0lHV3XjCs69F3OV2ce89qxqfG8n83nDlsz2lo7eScn78JQGNbF7+7bm73aCqPpvYurn54LeF2Gy99/Zxhy4tS3vQW9HX0Tj+ICJMz4pic0fc4buXddQvG8dquKt4urmHpdN+OenG6DDarmen1j6qoaGjjB5cP7xK8CVHh3HBOAb98vZjPFOWcEvDB3d/xyVlZ3PvvPVQfb/ParKeUv2nQV35x1vhk4iLDeH1XlU+Dfk1jO5f9+h2iI8K4YnYWa4pryE6M4oKp6T57Rm9WLCogNc7BFbN7b7pZMiWNe/+9h9V7qvnsvLHDniel+qLLMCi/CLfbWDw5jdV7qnG5fNOkaIxxj3pq6SQtzsGv3tjLB0fq+fyCsT7pvO5LVISdz5059pQRSz1NyYgjMyGSN3dXD3t+lOoPDfrKby6cmkZtUwdbBrlg2d82HGHZA2u6F4R7YmMJb+6u5o5PTOGp/ziLtXcs4f7PzOIrC0fPRB4RYcmUNN7ZW0t7V+8L3SnlLxr0ld8snpSG3Sa8vmvgi7kBPPtBKbsrG7n2D+u587nt/OjlXZwzIYXrz8oDIDMhik+dkTPqNi5fMiWNlg4nGw8OfrE8pXxFg77ym4TocOblJfHGRwNv6mhq72JrST1fXpjHFxaM44mNR4gIs/GLa2aNqsXcvDl7fAqOMNugyq2Ur2nQV3514dR09lQ1UnK0ZUDXbTp4lC6X4cKp6fzoykKeueks/nrDmQGx6FlUhJ2zxyfz5u5qXSJYjTgN+sqvPKNqXh/gev1r99cSEWZj7rgkAIryxlCYneDz/A2XJVPSOHK0xevSIH1Zvaeatk7tD1C+0WfQF5HHRKRaRHaclP51EdkjIjtF5J4e6XeKyD7r2NIe6XNFZLt17EEZLUsuKr/KT4lhfGoMz28pZ9WuKlbtqqL0WN+1/vf21TF3bNKoa6/vr8WT0wB4b1/tgK7bePAoX/7TJh57b/TuQqYCS39q+iuBZT0TROR84ApgpjFmOvALK30asByYbl3zkIh4/pc+DKwAJlqvE+6pQscnCjO7VwZ1v94/7fnHmjvYVXGcs8cn+ymHvpeTFEVidDi7K48P6LqnNpUA8M9tFcORLRWC+pycZYxZIyJ5JyXfDPzMGNNunePpoboCeNJKPygi+4D5InIIiDfGrAMQkceBK4FXfVEIFVj+88KJfGJGBsbAS9vK+f3bByg91kJOkvdF5tZbO1UNdb2ckSQiTE6PY09lY7+vaWzr5JXtFSREhbOz/DgHa5vJt1YNVWqwBtumPwk4V0Q2iMjbIjLPSs8GSnqcV2qlZVvvT073SkRWiMhmEdmsu+UEn3C7jelZCRRmJ/AZa2OY1aeZvPTe/lpiIuzMzAmcNnxvpmTEUVzV1O/O3Je3VdDa6eRnn5oBwD+3lQ9n9lSIGGzQDwOSgAXAbcDTVhu9t3Z6c5p0r4wxjxhjiowxRampA9/dSAWOgpQY8pKjeeM0QX/t/jrm548Z8CYwo83kjHia2rsoPdbar/Of2lTCpPRYlhVmMHdcEi9rE4/ygcH+LyoFnjNuGwEXkGKl5/Y4Lwcot9JzvKSrEOeesZrO2v11tHR0daeXHG1hS0k9a4prOFDTzMIAbtrx8CzY158mnuKqRraU1POZIvc2lZfOyGR3pXuVV6WGYrBB/3lgCYCITAIigFrgRWC5iDhEJB93h+1GY0wF0CgiC6xvBF8EXhhq5lVwWDIljY4uF2v3udvu1+6r5bx7V3Plb9/ji49tBDhhv4JANSndvbb/nqq+g/7Tm0oItwtXzXG3gl4yIxOAV7ZrbV8NTZ8duSLyBLAYSBGRUuAu4DHgMWsYZwdwvXE3VO4UkaeBXUAXcIsxxjPA+GbcI4GicHfgaieuAmB+/hhiIuy8sbuahRNSuPMf2xmXHMP/XDYVQYiPCj9lw/dAFBcZTnZiFLv7qOmv21/HExuPcOHU9O69GjISIpmXl8TL28q7+0ESosJDfoMeNXD9Gb1zbS+Hruvl/J8AP/GSvhkY+E7UKuhFhNlYNCmV1buriYsM43BdC0+uWMCCgsAdotmbKRlx7DnNsM1XtlfwjSe3MC45mu9/ctoJxy6bmcVdL+5kwU/fACAjPpK1dywZ9ctQqNFF19NXo8L5U9J4dUclj6w5wLXzc4My4IO7Xf/t4ho6ulxEhNnYUdbAo+8exGUM7Z0u/r2rkrljk/jj9UUkRkeccO1n5+US6wijw+liW2kDT2w8woHaJiak6eY+qv806KtR4XxrxmpqnIM7PjF1hHMzfCZnxNHlMhyobWJKRjw//ucutpTUk2HtqnXVnGz+96oZXmceR4bb+fRc93iIeXmNPLHxCB8eqdegrwZEg74aFVLjHHzv0qnMzEkkISp8pLMzbDx9E3sqG3G6DOsPHOU7l0xhxaLxA7pPQUoscY4wtpTUc01Rbt8XKGXRoK9GjRvOLRjpLAy7/JQYwmzC7spG1hTXEh1hH9Q2ijabMDM3gS0l9b7PpApqgT3bRakAExFmY3xqLO/tq+WlreVcMzdn0N9sZucmsruykdYOXYFT9Z8GfaX8bHJGHNtKG+h0ufjSELZ2nJ2bhNNl2FHe4MPcqWCnQV8pP/PMzL1gStqQFlCbnZsIwJYj9T7IlQoVGvSV8rM5YxMRgRuH2IeRGucgOzFq0BvNq9CkHblK+dnZ41PYcOcFpMUPfavH2bmJWtNXA6I1faVGgC8CPriDfll9KzWN7T65nwp+GvSVCmCzxyYC6NBN1W8a9JUKYIVZCdhtwpaSYyOdFRUgNOgrFcCiIuxMyYhja4kO21T9o0FfqQA3JSO+X2v0KwUa9JUKeJPSY6lpbKe+pWOks6ICgAZ9pQLcpHT3ZK+9upWi6gcN+koFuInWNozF2sSj+kGDvlIBLjsxipgIO3urtKav+qZBX6kAJyJMSIvVmr7qFw36SgWBielx2qav+kWDvlJBQEfwqP7SoK9UEJhojeAp1nZ91QcN+koFgUndQV/b9dXpadBXKghkJUQS6whjn7brqz5o0FcqCOgIHtVfGvSVChIT02K1TV/1SYO+UkFiUnoctU3tHGvWETyqdxr0lQoSnuUYdLy+Op0+98gVkceAy4BqY0yhlXY3cCNQY532HWPMKyKSB3wE7LHS1xtjbrKumQusBKKAV4D/NMYYn5VEqRDnGcHz3AellB5rOeFYUkwE509OG4lsqVGmPxujrwR+Azx+UvovjTG/8HL+fmPMbC/pDwMrgPW4g/4y4NV+51QpdVqZCZFkxEfy5KYSntxUcsrx17+5iAlpcSOQMzWa9Bn0jTFrrBr8oIlIJhBvjFlnfX4cuBIN+kr5jIiw6puLOHpSm37J0Vaue3QDW0saNOirIbXp3yoi20TkMRFJ6pGeLyIfisjbInKulZYNlPY4p9RK80pEVojIZhHZXFNT09tpSqmTxEWGMy455oTXWeOTiY6ws71Mt1RUgw/6DwPjgdlABXCflV4BjDXGzAG+CfxNROIB8XKPXtvzjTGPGGOKjDFFqampg8yiUgrAbhOmZcZr0FfAIIO+MabKGOM0xriAPwDzrfR2Y0yd9f59YD8wCXfNPqfHLXKA8qFkXCnVf4XZCewqP47TpWMnQt2ggr7VRu9xFbDDSk8VEbv1vgCYCBwwxlQAjSKyQEQE+CLwwpByrpTqt5k5CbR2Otlfo8M5Q11/hmw+ASwGUkSkFLgLWCwis3E30RwC/sM6fRHwQxHpApzATcaYo9axm/l4yOaraCeuUn4zIzsBgO2lDd1DO1Vo6s/onWu9JD/ay7nPAs/2cmwzUDig3CmlfKIgNba7M/fTc3P6vkAFLZ2Rq1QI0M5c5aFBX6kQMSNHO3OVBn2lQsaMbO3MVRr0lQoZPTtzVejSoK9UiOjZmatClwZ9pUKEduYq0KCvVEhZUJDM+4eP8eTGIyOdFTVCNOgrFUK+fsEEFk9O5c5/bOfpzacuvzxQq3dXn7KqpxrdNOgrFUIcYXZ+d91czpmQwu3PbuPFrYNfAut4Wydf+fMm/rLusA9zqIabBn2lQkxkuJ0/fLGIwqwEHlhVPOj7lB5txRhO2aVLjW4a9JUKQZHhdq6em8OB2mYODHLcfll96wl/qsCgQV+pEHXBVPeeuW98VD2o68usGn65Bv2AokFfqRCVkxTNlIw4Xv+oalDXe2r45fVtuHRph4ChQV+pEHbB1DQ2Hz5GQ0vngK/1BP0Op4va5nZfZ00NEw36SoWwC6am43QZ3ioeeBNP2bFWwmzS/V4FBg36SoWw2TmJpMRG8Pog2vXL6lsptNbzKa9v83XW1DDRoK9UCLPZhPMnp/HWnmo6na5+X9fW6aS2qYP5+WMA7cwNJBr0lQpxF0xNp7Gti/UH6vp9jac9f2pmHLGOMB22GUA06CsV4s6dmEJ8ZBhfWbmJW/76AWv31fZ5jacNPzsxmqzESA36AUSDvlIhLsYRxou3nsP1Z+Xx3v5aPvfHDby87fTLM3iCfHZSFNmJUdqRG0A06CulyEuJ4XuXTWP9nRcwKT2WB9/Ye9qx92XHWrHbhPQ4B1mJUZQ3aNAPFBr0lVLdIsPtfG3xBIqrmlh1mklbZfWtZMRHEma3kZ0URX1LJ83tXX7MqRosDfpKqRNcNjOTccnR/Hb1PozxXtsvO9ZKdlIUANmJ7j91BE9g0KCvlDpBmN3GzeeNZ1tpA+/s9d6pW1bfSk7iiUFfO3MDQ9hIZ0ApNfp86owcfvXGXn7x2h4qG9wTryamxzJnbBJdTheVx9u6a/pZGvQDigZ9pdQpIsJs3HL+BL73/A6+XboNgKhwO2vvWEJzRxdOl+mu4afFObDb5ITmnWPNHXS63JO9EqMiiAjTRoXRQoO+Usqrz585loumpdPlMpQcbWH5I+t5fN1hFhS4Z+F6avphdhsZ8ZHdwzZXvneQu1/a1X2fWTkJvHDrOf4vgPKqz1+/IvKYiFSLyI4eaXeLSJmIbLFel/Q4dqeI7BORPSKytEf6XBHZbh17UETE98VRSvmKiJAeH0l2YhQLCpJZMiWNlWsPss/adMVT0/e8L69v41hzB/etKmZ+3hh+fGUhV87OYmtpQ0B18t7/2h4+PHJspLMxbPrznWslsMxL+i+NMbOt1ysAIjINWA5Mt655SETs1vkPAyuAidbL2z2VUqPUzYvHc6ylk4dW7wc+bssHd62/rL6V36zeR3N7Fz++qpDrFozja+dPAGBNcc2I5HmgSo+18OCb+/jHh2UjnZVh02fQN8asAY72835XAE8aY9qNMQeBfcB8EckE4o0x64x7DNjjwJWDzLNSagTMyxvD3HFJlNW3khLrIDLc3n0sKzGSioZW/rLuMNfMzWVSehwAE9NiyUyI5O0ACfrrD7hDXUVD8K4aOpTelVtFZJvV/JNkpWUDJT3OKbXSsq33J6crpQLITeeNBz5uz/fITozGZcBmg/+6aFJ3uohw3qRU3t1bO6BVPEfKuv3uRecqNeif4mFgPDAbqADus9K9tdOb06R7JSIrRGSziGyuqQmMGoJSoeCCKWnMzEmgMCv+hHTPL4GvnpNPRkLkCccWTUqlsb2LLSX1/srmoBhjulcaDeaa/qBG7xhjuudni8gfgJetj6VAbo9Tc4ByKz3HS3pv938EeASgqKhIN99UapSw2YRnbz67e8csj7MKkrn7k9P4zLzcU65ZOCEFu01YU1zDvLwxvd7b6TJ8cOQY+6qbOFDTRH0fWzg6jaG1w0lTexftXR9/i7hoajo3LioYYMmg5GgrZfWtpMY5qGlsp6PLFZRDTQcV9EUk0xhTYX28CvCM7HkR+JuI3A9k4e6w3WiMcYpIo4gsADYAXwR+PbSsK6VGQrj91EAYEWbjSwvzvZ6fEBXOnNxE3i6u4b8vntzrff/03kF+/M+Puu+XHBPhtYnAQ0SIjrAT4wjDEWZDxN0sc+9re7h6bg5JMREDKpenln/5rCweffcgVcfbyB0TPaB7BII+g76IPAEsBlJEpBS4C1gsIrNxN9EcAv4DwBizU0SeBnYBXcAtxhindaubcY8EigJetV5KqRBw3qRU7n+9mLqmdpJjHV7PKa5qJDkmgudvWUhWYhR228BHde+uPM6yB97hmfdLB1zbX3egjpTYCM6dmMKj7x6kMlSDvjHmWi/Jj57m/J8AP/GSvhkoHFDulFJB4bzJqdy3qpiXtpazZEo6druQlRBJz+k6JUdbGZccPaRAOyUjnnl5SfzfhsN89Zx8bP38xWGMYd3+Os4sSO4eihqs7frB12CllBp1CrMSSImN4O6XdrHo3tUs/NmbPPfBiWPhS461+KRmfd2CcRyua+HdfuwA5nGoroXK422cVZDc3RFdGaR7BOgyDEqpYWezCSu/PJ89lY0AfP+FHWwva+DTc93jO7qcLioa2shNGnrQX1aYQXJMBP+3/jCLJqWecKy9y8mWI/U4rQ1iYiPDmJwR192ev6AgmThHGDERdiob2oecl9FIg75Syi8KsxMozE4AYOXaQxyobe4+VtHQhtNlyB0T1dvl/eYIs3NNUS6PrNlPeX0rWYlRHG/r5G8bjvDYuwepbjwxmEfYbURF2EmNczA+NQYRISMhksrjWtNXSimfyE+J4YMe69uUHG0B8ElNH9yLxf1+zX4W3bMam03ocrpwGThnQgo/vKKQpOhwAOqaO9haUs/W0noWT07r7mPITIgK2jZ9DfpKKb8rSI3hpW3ltHU6iQy3U3LMCvo+Gi2TOyaa+66ZRXGVe3G4MJuwrDCj+5tGT5fMyDwlLSMhkvcG0CcQSDToK6X8Lj8lBmPgcF0LkzPiKDnq3mg986TZvEPxqTNy+j6pF5kJkVQ3ttPldBHmZV5CIAuu0iilAkJBSiwAB2vdNfGSYy1kJkSOmgCbkRCJ02WobeoY6az43Oj4CSulQkp+agwA+2vcnbklR1t81p7vC55vHBVBOGxTg75Syu9iHWGkxTk4aI3gKTnW6pORO76SEe/OSzCutqlBXyk1IgpSYzhQ00Rbp5OaxvZRWtPXoK+UUj6RnxLLwdpmSn08cscXEqPDcYTZqDyuQV8ppXyiICWGYy2dbC9rABhVzTsi7pFEWtNXSikfKbA6c9cUu8fDj6bmHXCP4AnG9Xc06CulRkR+iifo1+AIs5Ea533J5ZESrLNyNegrpUZE7phowmxCXXMHOUlRJyyzPBpkJERSdbwNlyu4Nu/ToK+UGhHhdhtjrc7b0dSJ65GZEEmn01Db1M7fN5fwnX9spysANnfviy7DoJQaMfkpMRyobR517fkAGfHuYZufengtpcfcbftfWZjPhLTYkczWkGlNXyk1YjyduaNp5I5HntXn4HIZvnR2HgA1jYG/xr7W9JVSIybfWoNnNNb0J6XH8fwtC5mcHkfpsRZWrj1ETZMGfaWUGrQzC8YwLjmaWbmJI50Vr2Zb+fKMLNKavlJKDcH41Fjevu38kc5GnxKiwgm3S1AEfW3TV0qpPogIqbEODfpKKRUqUuMcQdGmr0FfKaX6ITXOQXUQLMCmQV8ppfohNc5Brdb0lVIqNKTGRVLX3BHws3I16CulVD+kxjkwBo42B/a+uRr0lVKqH1Jj3WP1qwN8BE+fQV9EHhORahHZ4eXYt0TEiEiK9TlPRFpFZIv1+l2Pc+eKyHYR2SciD8poW1JPKaVOo3uCVoC36/enpr8SWHZyoojkAhcBR046tN8YM9t63dQj/WFgBTDRep1yT6WUGq3SgmRWbp9B3xizBjjq5dAvgW8DfS42LSKZQLwxZp0xxgCPA1cOLKtKKTVyUmJDJOh7IyKXA2XGmK1eDueLyIci8raInGulZQOlPc4ptdJ6u/8KEdksIptramoGk0WllPKpqAg7cY6wgA/6A157R0Sige8CF3s5XAGMNcbUichc4HkRmQ54a7/v9RuCMeYR4BGAoqKi4Nq2RikVsIJhVu5gavrjgXxgq4gcAnKAD0QkwxjTboypAzDGvA/sBybhrtnn9LhHDlA+lIwrpZS/pcQF/vo7Aw76xpjtxpg0Y0yeMSYPd0A/wxhTKSKpImIHEJEC3B22B4wxFUCjiCywRu18EXjBd8VQSqnhlxrnoDbAg36fzTsi8gSwGEgRkVLgLmPMo72cvgj4oYh0AU7gJmOMpxP4ZtwjgaKAV62XUkoFjNRYB2uGKejvr2niyt+8R0unsztt5w+WEhlu9+lz+gz6xphr+zie1+P9s8CzvZy3GSgcYP6UUmrUSI1z0NjeRWuHk6gI3wbjHWUNNLZ38YUF40iICgfAbvP9dCbdREUppfrJM0Grtqmd3DG+3eKxosG9guftn5hCrGP4QrMuw6CUUv3kmaA1HEsxVNS3EhcZNqwBHzToK6VUv328V67v19Uvb2gjKyHK5/c9mQZ9pZTqp+HcIL2ioZXMxEif3/dkGvSVUqqfkmMc2GSYgn59G5la01dKqdHDbhPGxPh+Vm5bp5O65g6yErSmr5RSo0rqMMzKrbL23s1MHP6avg7ZVEqpAUiNc1B5vI3Gtk4AYh1hDHV7kPJ6K+j7oaavQV8ppQYgI97BmuIaZtz9GgCfKcrhnqtnDemeFQ2tgAZ9pZQadb6+ZCKT0uMAeHN3Nf/cVsEPrygc0nIJnolZ2pGrlFKjTO6YaG44t4Abzi3gxkUFNHc4Wbe/bkj3LK9vJSk63OdLO3ijQV8ppQbp7PHJxETYeW1X1ZDuU9Hgn+GaoEFfKaUGzRFmZ/HkNF7/qAqXa/D7PVU0tJHlh4lZoEFfKaWG5KJp6dQ0trOltH7Q96hoaNWavlJKBYLzJ6cRZhNe2zm4Jp7WDif1LZ1k+GHkDmjQV0qpIUmIDufMgjGs2lU5qOvLreGa2ryjlFIB4qKp6eyvaWZ/TdOAr62o999wTdCgr5RSQ3bR9AwA3tpTM+BrPROz/LGsMmjQV0qpIctOjCLWEUbJ0ZYBX+uZmJWe4PB1trzSoK+UUj6QNsiF2CoaWkmJdeAIG/6JWaBBXymlfCI1zkH1IHbUKq9v88uaOx4a9JVSygfS4iMHtXeue4y+Bn2llAooaXEOqo+3Y8zAZuZW1LeR5Yd19D006CullA+kxTlo7XTS1N7V72ua2rtobO/Smr5SSgWatPiBb5ru2THLX7NxQYO+Ukr5RFqcO3APpF3fE/Q91/qDBn2llPKBtDh3TX8gQb/6uPvc9Hj/jNEHDfpKKeUT3TX94/0ftlnpqenHj6Kavog8JiLVIrLDy7FviYgRkZQeaXeKyD4R2SMiS3ukzxWR7daxB2WoOwkrpdQoEh8VRkSYbcBt+rGOMGId/tu5tj81/ZXAspMTRSQXuAg40iNtGrAcmG5d85CIeKaZPQysACZar1PuqZRSgUpE3MM2B9i8k+bHph3oR9A3xqwBjno59Evg20DPQalXAE8aY9qNMQeBfcB8EckE4o0x64x7EOvjwJVDzbxSSo0mA52VW3W8jQw/Nu3AINv0ReRyoMwYs/WkQ9lASY/PpVZatvX+5PTe7r9CRDaLyOaamoGvWqeUUiPBM0Grv6oa20gf7UFfRKKB7wLf93bYS5o5TbpXxphHjDFFxpii1NTUgWZRKaVGRFpc/5diMMZQNRqbd7wYD+QDW0XkEJADfCAiGbhr8Lk9zs0Byq30HC/pSikVNNLiHDS0dtLW6ezz3IbWTjq6XKT7cYw+DCLoG2O2G2PSjDF5xpg83AH9DGNMJfAisFxEHCKSj7vDdqMxpgJoFJEF1qidLwIv+K4YSik18gYyK9czXHPUNe+IyBPAOmCyiJSKyFd7O9cYsxN4GtgF/Au4xRjj+ZV3M/BH3J27+4FXh5h3pZQaVQYyK7dqBCZmAfQ5ONQYc20fx/NO+vwT4CdeztsMFA4wf0opFTBS4zw1/Y9H8DS0dOK0Vt6Miwwj3O6ua1eNUE3ffzMClFIqyHmadzw1/WfeL+Vbf/94kOOs3EReuGWh+xwr6Ht+UfiLBn2llPKR5BgHNvm4Tf8fH5aSkxTFjecWsG5/Hf/aWUlDaycJUeFUHW8nMTqcyHD/bJPooWvvKKWUj9htQkqse6z+seYO1h84yhWzs7j+7Dw+v2AsANtLGwB3846/R+6ABn2llPKptHj3rNxVH1XhdBmWTc8EYGZ2IgBbS+sBqGpsJ92P6+h7aNBXSikf8kzQ+veOSrIToyjMjgcgITqcgpQYtpTUA+42/XQ/t+eDBn2llPKptDgHJUdbeGdvLUunZ9BzQeFZuYlsK63H6TJUN7b7feQOaNBXSimfSo1zcLytiw6ni2WFGSccm5WTQNXxdnaVH8fpMn4fow8a9JVSyqc8O2ilxEYwd1zSCcdm5SYC8NquSve5WtNXSqnAlmqNyLloWgZ224lrTU7NjCfcLry2swrw/8Qs0KCvlFI+NTE9ljCbcNWcU1ePjwy3MzUznj1VjYD/l2AADfpKKeVT41Nj2X73Uubnj/F6fFZOIgAikBKrQV8ppQJeVETvs2w97frJMY7udXj8SYO+Ukr50ezcBGBkmnZAg75SSvlVQUossY6wEenEBV1wTSml/MpmE75/2TSyEqNG5Pka9JVSys8+My+375OGiTbvKKVUCNGgr5RSIUSDvlJKhRAN+kopFUI06CulVAjRoK+UUiFEg75SSoUQDfpKKRVCxBgz0nk4LRGpAQ4P4JIUoHaYsjMaaPkCm5YvcAVa2cYZY1JPThz1QX+gRGSzMaZopPMxXLR8gU3LF7iCpWzavKOUUiFEg75SSoWQYAz6j4x0BoaZli+wafkCV1CULeja9JVSSvUuGGv6SimleqFBXymlQkhQBX0RWSYie0Rkn4jcMdL56Y2I5IrIahH5SER2ish/WuljRGSViOy1/kzqcc2dVrn2iMjSHulzRWS7dexBEREr3SEiT1npG0Qkz89ltIvIhyLychCWLVFEnhGR3dbf4VlBVr7/sv5d7hCRJ0QkMpDLJyKPiUi1iOzokeaX8ojI9dYz9orI9cNZzn4zxgTFC7AD+4ECIALYCkwb6Xz1ktdM4AzrfRxQDEwD7gHusNLvAH5uvZ9mlccB5FvltFvHNgJnAQK8CnzCSv8a8Dvr/XLgKT+X8ZvA34CXrc/BVLY/AzdY7yOAxGApH5ANHASirM9PA18K5PIBi4AzgB090oa9PMAY4ID1Z5L1Psmf/1a9/jxGOgM+/Is9C/h3j893AneOdL76mfcXgIuAPUCmlZYJ7PFWFuDfVnkzgd090q8Fft/zHOt9GO6ZhOKn8uQAbwBL+DjoB0vZ4nEHRTkpPVjKlw2UWIEqDHgZuDjQywfkcWLQH/by9DzHOvZ74Fp//D2e7hVMzTuef6wepVbaqGZ9FZwDbADSjTEVANafadZpvZUt23p/cvoJ1xhjuoAGIHlYCnGqB4BvA64eacFStgKgBviT1Xz1RxGJIUjKZ4wpA34BHAEqgAZjzGsESfl68Ed5RmVMCqagL17SRvV4VBGJBZ4FvmGMOX66U72kmdOkn+6aYSUilwHVxpj3+3uJl7RRWTZLGO6mgoeNMXOAZtzNA70JqPJZbdtX4G7ayAJiROS6013iJW3Ulq8ffFmeUVnOYAr6pUDPLeZzgPIRykufRCQcd8D/qzHmOSu5SkQyreOZQLWV3lvZSq33J6efcI2IhAEJwFHfl+QUC4HLReQQ8CSwRET+j+Aom+fZpcaYDdbnZ3D/EgiW8l0IHDTG1BhjOoHngLMJnvJ5+KM8ozImBVPQ3wRMFJF8EYnA3aHy4gjnySur1/9R4CNjzP09Dr0IeHr4r8fd1u9JX26NEsgHJgIbra+ljSKywLrnF0+6xnOvq4E3jdWwOJyMMXcaY3KMMXm4/w7eNMZcFwxlAzDGVAIlIjLZSroA2EWQlA93s84CEYm28nUB8BHBUz4Pf5Tn38DFIpJkfYO62EobWSPdqeDLF3AJ7pEw+4HvjnR+TpPPc3B/zdsGbLFel+BuB3wD2Gv9OabHNd+1yrUHa9SAlV4E7LCO/YaPZ1lHAn8H9uEedVAwAuVczMcduUFTNmA2sNn6+3se98iMYCrfD4DdVt7+gnskS8CWD3gCd/9EJ+7a91f9VR7gK1b6PuDL/v4/6O2lyzAopVQICabmHaWUUn3QoK+UUiFEg75SSoUQDfpKKRVCNOgrpVQI0aCvgo64V8H8mvU+S0SeGcZnzRaRS4br/kr5mgZ9FYwSca98iDGm3Bhz9TA+azbuORZKBQQdp6+Cjog8iXv9mD24J99MNcYUisiXgCtxL8NdCNyHe2nkLwDtwCXGmKMiMh74LZAKtAA3GmN2i8g1wF2AE/eiWhfinnQTBZQBP8W9AucDVlor7gk5ewbw7LdwT9abj3tFz68YYzb6/qekQtZIzw7Tl758/aLHMronvf8S7iAdhzugNwA3Wcd+iXvhO3DP0JxovT8T97R6gO1AtvU+scc9f9Pj2fFAmPX+QuDZAT77LeAP1vtF9FgOWF/68sUrzFe/PJQKEKuNMY2411FpAF6y0rcDM62VT88G/m5tjATuZQgA3gNWisjTuBci8yYB+LOITMS91EZ4f5/d47wnAIwxa0QkXkQSjTH1gyuuUifSoK9CTXuP964en124/z/YgHpjzOyTLzTG3CQiZwKXAltE5JRzgB/hDu5XWXslvDWAZ3c/6uRH914cpQZGO3JVMGrE3YwyYMa9r8FBq/0ecZtlvR9vjNlgjPk+7t2Rcr08KwF3+z64m3QG47PW887BvYlJwyDvo9QpNOiroGOMqQPeszbCvncQt/g88FUR2QrsxN0pDHCvtTH2DmAN7r1UVwPTRGSLiHwW996rPxWR93B32g7GMRFZC/wO94qQSvmMjt5RahSxRu98yxizeaTzooKT1vSVUiqEaE1fKaVCiNb0lVIqhGjQV0qpEKJBXymlQogGfaWUCiEa9JVSKoT8f4zqYkKdLNHkAAAAAElFTkSuQmCC\n",
      "text/plain": [
       "<Figure size 432x288 with 1 Axes>"
      ]
     },
     "metadata": {
      "needs_background": "light"
     },
     "output_type": "display_data"
    }
   ],
   "source": [
    "from dlmontepython.htk.sources import dlptfile\n",
    "import pandas as pd\n",
    "\n",
    "yaml_out = dlptfile.load_yaml('./tutorial_2_simulation/')\n",
    "yaml_df = pd.DataFrame(yaml_out.data)\n",
    "yaml_df = yaml_df.set_index('timestamp')\n",
    "yaml_df.plot.line(y='volume')"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "d271a923",
   "metadata": {},
   "source": [
    "### Questions to ask yourself:\n",
    "* Would it help equilibration in the *NpT* ensemle to start the simulation after *NVT* equilibration?\n",
    "* What would be the effect of changing pressure and/or temperature?\n",
    "* How to *restart* the simulation for continuation?\n",
    "\n",
    "Try to increase the pressure and run a longer simulation starting with the last saved configuration (i.e. REVCON.000). "
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ff3962ee",
   "metadata": {},
   "source": [
    "Alternative V-sampling schemes\n",
    "------------------------------\n",
    "\n",
    "It is also possible to conduct a random walk in the logarithm of the volume, by using the keyword **log** instead of **linear** in the directive **move volume**. This changes the way in which the move is generated: :math:`\\ln(V)` is varied rather than linear dimensions of the simulation cell. This type of V-sampling is often claimed to be more efficient due to direct *scaling* of volume. \n",
    "\n",
    "The acceptance probability for the volume moves is now\n",
    "\n",
    "\n",
    "$$\n",
    "  P_{\\mathrm{acc}}([\\mathbf{r}_{1},V_1] \\rightarrow [\\mathbf{r}_2,V_2]) = \\min(1, \\exp \\{- \\beta [U(\\mathbf{r}_2) - U(\\mathbf{r}_1) + P_{ext}(V_{2}-V_{1}) - ( N + 1 ) \\beta^{-1} \\ln(V_{2} / V_{1}) ] \\} )\n",
    "$$\n",
    "\n",
    "Often simulation cells will be orthorhombic (but not cubic), and more generally mono-/tri-clinic. For setting a more general volume move in these cases refer to the manual.\n",
    "         \n",
    "The fraction of accepted volume moves is affected by the directive **maxvolchange** setting the value of the *volume variation parameter* $\\Delta_V$. In the course of an NpT simulation this value is though automatically adjusted so that the acceptance ratio converges to the targeted value that can be set in the CONTROL file by the directive **acceptatmmoveratio \\<target ratio>** (0.37 by default). Additional directive **acceptvolupdate \\<stride>** determines the frequency of adjustments for $\\Delta_V$. \n",
    "\n",
    "\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "9b02f315",
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "dlmontepython",
   "language": "python",
   "name": "dlmontepython"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.2"
  },
  "toc": {
   "base_numbering": 1,
   "nav_menu": {},
   "number_sections": true,
   "sideBar": true,
   "skip_h1_title": true,
   "title_cell": "Table of Contents",
   "title_sidebar": "Contents",
   "toc_cell": false,
   "toc_position": {},
   "toc_section_display": true,
   "toc_window_display": true
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
