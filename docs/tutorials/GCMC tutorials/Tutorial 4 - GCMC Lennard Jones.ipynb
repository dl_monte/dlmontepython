{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "facaedbc",
   "metadata": {},
   "source": [
    "TUTORIAL 4 : GCMC Lennard-Jones\n",
    "===============================\n",
    "\n",
    "Original authors: James Grant (r.j.grant{at}bath.ac.uk), Tom L. Underwood (t.l.underwood{at}bath.ac.uk)\n",
    "\n",
    "Modified to use dlmontepython by Joe Manning (joseph.manning{at}manchester.ac.uk)\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ea486a8d",
   "metadata": {},
   "source": [
    "Introduction\n",
    "------------\n",
    "\n",
    "Grand Canonical Monte Carlo (GCMC) is so named because it allows sampling from the grand canonical ensemble.  \n",
    "In NVT we couple the simulation to a thermostat, with which the system exchanges energy at constant temperature.\n",
    "Similarly with NPT we introduce a barostat which allows volume exchange at constant pressure.\n",
    "GCMC marks a distinction from ensembles that can be modelled with molecular dynamics (MD) because we now allow particles to be added or deleted from the system.\n",
    "This exchange happens at constant *chemical potential* $\\mu$ and the ensemble is alternatively known as \n",
    "$\\mu\\mathrm{VT}$ since volume and temperature are also held constant.\n",
    "The total energy, $E$ of the system is now defined as\n",
    "\n",
    "\n",
    "$$\n",
    "  E=U(\\underline{r})+\\mu N\n",
    "$$\n",
    "where $U(\\underline{r})$ is the energy of the configuration and *N* the number of particles."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ea6c0c5f",
   "metadata": {},
   "source": [
    "Inputs\n",
    "------"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "c4bc4af7",
   "metadata": {},
   "source": [
    "### Setting up the CONTROL file \n",
    "\n",
    "The CONTROL file is a bit different in GCMC compared to NVT or NVP MC. We'l;l modify our control file as below.\n",
    "\n",
    "```\n",
    "   GCMC Lennard-Jones \n",
    "   finish\n",
    "   seeds 12 34 56 78               # Seed RNG seeds explicitly to the default\n",
    "   temperature     1.4283461511745 # Corresponds to T*=1.1876; \n",
    "                                   # T(in K) = T* / BOLTZMAN \n",
    "\n",
    "   steps          10000            # Number of moves to perform in simulation\n",
    "   equilibration    0              # Equilibration period: statistics \n",
    "                                   # are gathered after this period\n",
    "   print           1000            # Print statistics every 'print' moves\n",
    "   stack           1000            # Size of blocks for block averaging to obtain statistics\n",
    "   sample coord   10000            # How often to print configurations to ARCHIVE.000\n",
    "   revconformat dlmonte            # REVCON file is in DL_MONTE CONFIG format\n",
    "   archiveformat dlpoly4           # ARCHIVE.000/HISTORY.000/TRAJECTORY.000 format \n",
    "                                   # In this case: HISTORY.000 in DLPOLY4 style\n",
    "   yamldata 1000\n",
    "   move gcinsertmol 1 100 0.7      # Perform insertion/removal moves for lj, a weight 100\n",
    "                                   # with a min. distance of 0.7 from atoms for inserts\n",
    "   lj  0.06177                     # Use an activity of 0.06177   \n",
    "   #  move atom 1 512                 # Move atoms with a weight of 512\n",
    "   #  LJ core \n",
    "   #  move volume cubic linear 1      # Move volume, box is cubic, \n",
    "   #                                # linear scaling with a weight of 1\n",
    "   start\n",
    "```\n",
    "You'll first notice we removed the directives that switch on the neighbour lists *nbrlist* and *maxnonbondnbrs* (and *verlet*).\n",
    "The cost of maintaining the list in GCMC negates the benefits when calculating the energy.\n",
    "In this calculation DL_MONTE is using the activity *a* rather than the chemical potential $\\mu$,\n",
    "which are related according to \n",
    "\n",
    "$$\n",
    "a = \\exp(\\mu/RT)\n",
    "$$\n",
    "\n",
    "where *R* is the gas constant.\n",
    "Note that the grand-canonical insert/delete moves are performed on the *molecule* level.\n",
    "\n",
    "Here we have turned off atom translation moves though there is nothing in principle wrong with allowing these types of moves.\n",
    "However in order to work in the $\\mu\\mathrm{VT}$ ensemble volume moves *MUST* be switched off.\n",
    "Consider what could happen if both insert/deletes and volume moves were allowed.\n",
    "The acceptance rule for linear displacement using the Metropolis approach for insertions is:\n",
    "\n",
    "$$\n",
    "P_{\\mathrm{acc}}([\\mathbf{r}_1,N_1] \\rightarrow [\\mathbf{r}_2,N_2] ) = \\min(1,  \\frac{V\\Lambda^{-3}}{N+1} \\exp \\{- \\beta [E(\\mathbf{r}_2,N_2) - E(\\mathbf{r}_1,N_1)] \\} )\n",
    "$$\n",
    "and for deletions is:\n",
    "\n",
    "$$\n",
    "  P_{\\mathrm{acc}}([\\mathbf{r}_1,N_1] \\rightarrow [\\mathbf{r}_2,N_2] ) = \\min(1,  \\frac{N}{V\\Lambda^{-3}}\\exp \\{- \\beta [E(\\mathbf{r}_2,N_2) - E(\\mathbf{r}_1,N_1)] \\} )\n",
    "$$\n",
    "\n",
    "#### Exercise 1: setup the CONTROL file now in the `Tutorial_4` directory using dlmontepython.\n",
    "> Hint, refer to the previous tutorials for code to be reused, or entire CONTROL files to read in and modify! "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "id": "fe74accf",
   "metadata": {
    "ExecuteTime": {
     "end_time": "2022-08-10T16:19:12.565368Z",
     "start_time": "2022-08-10T16:19:12.119382Z"
    }
   },
   "outputs": [],
   "source": [
    "from dlmontepython.htk.sources import dlcontrol\n",
    "# Your code here!"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "3302fde6",
   "metadata": {},
   "source": [
    "### The FIELD and CONFIG files\n",
    "The FIELD and CONFIG files need slight changes compared to tutorial 3 in order to work with GCMC in DL_MONTE.\n",
    "First we will consider the FIELD file:\n",
    "\n",
    "```\n",
    "   Lennard-Jones, 2.5*sigma cut-off, sigma = 1 angstrom, epsilon = 1eV\n",
    "   CUTOFF 2.5\n",
    "   UNITS internal\n",
    "   NCONFIGS 1\n",
    "   ATOMS 1\n",
    "   LJ core 1.0  0.0\n",
    "   MOLTYPES 1\n",
    "   lj\n",
    "   ATOMS 1 1\n",
    "   LJ core 0.0 0.0 0.0\n",
    "   FINISH\n",
    "   VDW 1\n",
    "   LJ core  LJ core lj   1.0 1.0\n",
    "   CLOSE\n",
    "```\n",
    "Although the *cutoff* and *units* and number of configurations *nconfigs* remain the same as in NVT and NPT exercises, and there is still 1 atom type and 1 molecule type, the FIELD the block declaring the *molecules* is different from what was used previously.\n",
    "\n",
    "In the NVT and NPT cases all the particles were declared to be part of the same molecule. In GCMC each particle should be considered a `molecule` in its own right. To this end, the *maxatom* directive that we used before is now replaced with another directive: `*atoms <initial atoms> <max atoms>*` which provides a template for the atoms' coordinates (if there were more atoms in a molecule then the coordinates for all of them would have to be specified).\n",
    "\n",
    "In principle atoms can be added or removed from a molecule. However in the cases we will consider only molecule will be inserted or deleted, so molecules will have a fixed number of particles, in this case 1. We also define an archetype of the molecule listing the atoms comprising the molecule and their relative coordinates. Again, since we have a single Lennard--Jones particle in each molecule we simply position it at the origin of the reference molecule. The molecule entry is completed with the *finish* directive and the file is completed with the interactions, in this case a single *vdw* interaction as before.\n",
    "\n",
    "Turning to the CONFIG, this is also altered to take account of the changes in the molecule declaration given in the FIELD file:\n",
    "\n",
    "```\n",
    "   Lennard-Jones starting configuration rho = 0.125; particles are molecules, not atoms\n",
    "         0         1\n",
    "   10.0000000000000000  0.0000000000000000  0.0000000000000000\n",
    "   0.0000000000000000  10.0000000000000000  0.0000000000000000\n",
    "   0.0000000000000000  0.0000000000000000  10.0000000000000000\n",
    "   NUMMOL 8 1000\n",
    "   MOLECULE lj 1 1\n",
    "   LJ   core\n",
    "   -5.0000000000000000 -5.0000000000000000 -5.0000000000000000 \n",
    "   MOLECULE lj 1 1\n",
    "   LJ   core\n",
    "   0.0000000000000000 -5.0000000000000000 -5.0000000000000000 \n",
    "   etc\n",
    "```\n",
    "The molecule declaration mirrors that seen in the FIELD file.\n",
    "Each particle must be declared as part of a molecule along with its type and initial and maximum numbers of atoms.\n",
    "The atom/particle declaration and its position then follows as before."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "91862062",
   "metadata": {},
   "source": [
    "#### Exercise 2: Set up CONFIG and FIELD files using dlmontepython"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "id": "0018bd6a",
   "metadata": {
    "ExecuteTime": {
     "end_time": "2022-08-10T16:21:06.268630Z",
     "start_time": "2022-08-10T16:21:02.823647Z"
    }
   },
   "outputs": [],
   "source": [
    "from dlmontepython.htk.sources import dlfield, dlconfig\n",
    "# your code here!"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "8a9b1157",
   "metadata": {},
   "source": [
    "Analysing the outputs\n",
    "---------\n",
    "Launch DL_MONTE as you did in the previous tutorial. Again, we'll use dlmontepython to analyse the results from the YAMLDATA file."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "id": "4add2005",
   "metadata": {
    "ExecuteTime": {
     "end_time": "2022-08-11T10:22:55.816637Z",
     "start_time": "2022-08-11T10:22:55.732640Z"
    }
   },
   "outputs": [
    {
     "data": {
      "text/html": [
       "<div>\n",
       "<style scoped>\n",
       "    .dataframe tbody tr th:only-of-type {\n",
       "        vertical-align: middle;\n",
       "    }\n",
       "\n",
       "    .dataframe tbody tr th {\n",
       "        vertical-align: top;\n",
       "    }\n",
       "\n",
       "    .dataframe thead th {\n",
       "        text-align: right;\n",
       "    }\n",
       "</style>\n",
       "<table border=\"1\" class=\"dataframe\">\n",
       "  <thead>\n",
       "    <tr style=\"text-align: right;\">\n",
       "      <th></th>\n",
       "      <th>timestamp</th>\n",
       "      <th>energy</th>\n",
       "      <th>energyvdw</th>\n",
       "      <th>nmol</th>\n",
       "    </tr>\n",
       "  </thead>\n",
       "  <tbody>\n",
       "    <tr>\n",
       "      <th>0</th>\n",
       "      <td>1000</td>\n",
       "      <td>-6.000009e+07</td>\n",
       "      <td>-6.000009e+07</td>\n",
       "      <td>[107]</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>1</th>\n",
       "      <td>2000</td>\n",
       "      <td>-6.000014e+07</td>\n",
       "      <td>-6.000014e+07</td>\n",
       "      <td>[132]</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>2</th>\n",
       "      <td>3000</td>\n",
       "      <td>-6.000023e+07</td>\n",
       "      <td>-6.000023e+07</td>\n",
       "      <td>[176]</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>3</th>\n",
       "      <td>4000</td>\n",
       "      <td>-6.000020e+07</td>\n",
       "      <td>-6.000020e+07</td>\n",
       "      <td>[166]</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>4</th>\n",
       "      <td>5000</td>\n",
       "      <td>-6.000021e+07</td>\n",
       "      <td>-6.000021e+07</td>\n",
       "      <td>[172]</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>5</th>\n",
       "      <td>6000</td>\n",
       "      <td>-6.000023e+07</td>\n",
       "      <td>-6.000023e+07</td>\n",
       "      <td>[167]</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>6</th>\n",
       "      <td>7000</td>\n",
       "      <td>-6.000028e+07</td>\n",
       "      <td>-6.000028e+07</td>\n",
       "      <td>[191]</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>7</th>\n",
       "      <td>8000</td>\n",
       "      <td>-6.000025e+07</td>\n",
       "      <td>-6.000025e+07</td>\n",
       "      <td>[178]</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>8</th>\n",
       "      <td>9000</td>\n",
       "      <td>-6.000023e+07</td>\n",
       "      <td>-6.000023e+07</td>\n",
       "      <td>[170]</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>9</th>\n",
       "      <td>10000</td>\n",
       "      <td>-6.000025e+07</td>\n",
       "      <td>-6.000025e+07</td>\n",
       "      <td>[176]</td>\n",
       "    </tr>\n",
       "  </tbody>\n",
       "</table>\n",
       "</div>"
      ],
      "text/plain": [
       "   timestamp        energy     energyvdw   nmol\n",
       "0       1000 -6.000009e+07 -6.000009e+07  [107]\n",
       "1       2000 -6.000014e+07 -6.000014e+07  [132]\n",
       "2       3000 -6.000023e+07 -6.000023e+07  [176]\n",
       "3       4000 -6.000020e+07 -6.000020e+07  [166]\n",
       "4       5000 -6.000021e+07 -6.000021e+07  [172]\n",
       "5       6000 -6.000023e+07 -6.000023e+07  [167]\n",
       "6       7000 -6.000028e+07 -6.000028e+07  [191]\n",
       "7       8000 -6.000025e+07 -6.000025e+07  [178]\n",
       "8       9000 -6.000023e+07 -6.000023e+07  [170]\n",
       "9      10000 -6.000025e+07 -6.000025e+07  [176]"
      ]
     },
     "execution_count": 6,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "from dlmontepython.htk.sources import dlptfile\n",
    "import pandas as pd\n",
    "\n",
    "yaml_out = dlptfile.load_yaml('./Tutorial_4_files/')\n",
    "yaml_df = pd.DataFrame(yaml_out.data)\n",
    "yaml_df\n",
    "#yaml_df = yaml_df.set_index('timestamp')\n",
    "#yaml_df.plot.line(y='nmols')"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "2d783f72",
   "metadata": {},
   "source": [
    "You'll ntice that instead of being an integer, 'nmol' here is a list. This comes in handy when you have multiple molecules - the index of the list matches the order they're defined in the `FIELD` file.\n",
    "\n",
    "To make use of the information, we need to change the data type with df.apply"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 13,
   "id": "631dc160",
   "metadata": {
    "ExecuteTime": {
     "end_time": "2022-08-11T10:30:21.645731Z",
     "start_time": "2022-08-11T10:30:19.812921Z"
    }
   },
   "outputs": [
    {
     "data": {
      "text/plain": [
       "<AxesSubplot:xlabel='timestamp'>"
      ]
     },
     "execution_count": 13,
     "metadata": {},
     "output_type": "execute_result"
    },
    {
     "data": {
      "image/png": "iVBORw0KGgoAAAANSUhEUgAAAXgAAAEGCAYAAABvtY4XAAAAOXRFWHRTb2Z0d2FyZQBNYXRwbG90bGliIHZlcnNpb24zLjUuMSwgaHR0cHM6Ly9tYXRwbG90bGliLm9yZy/YYfK9AAAACXBIWXMAAAsTAAALEwEAmpwYAAAqLklEQVR4nO3dd3yV5f3/8deVTSDsQBICSZAZVgQM4ECqKEMFt2hddVVr+21ttWpt609bax1FbW1ddVS0VJwFJSCiAipDUEYggbACIRMCIYTsXL8/zlEjDWSd5D7j/Xw8zoM711mfcwfe3Oe6rvu6jbUWERHxP0FOFyAiIm1DAS8i4qcU8CIifkoBLyLipxTwIiJ+KsTpAgB69uxpExMTnS5DRMSnrFu3br+1Nvp493tFwCcmJrJ27VqnyxAR8SnGmOwT3a8uGhERP6WAFxHxUwp4ERE/5RV98CIizVFdXU1OTg4VFRVOl9IuIiIiiI+PJzQ0tFnPU8CLiM/JyckhKiqKxMREjDFOl9OmrLUcOHCAnJwckpKSmvVcddGIiM+pqKigR48efh/uAMYYevTo0aJvKwp4EfFJgRDu32jpZ1XAiwSAssoa/r16D0erapwuRdqRAl4kADyyKJPfvLuJq15YTXFZldPlyAl06tTJY6+lgBfxc+n7SnhtVTYT+vdgS95hLn32C3IOHnW6LGkHCngRP1ZXZ/n9f9PpFhnGs9eMYc4NqRSVVnLJM1+QmX/Y6fJ82u7duxk6dCg333wzw4YN49xzz6W8vJxJkyZxxx13MHHiRIYOHcqXX37JxRdfzMCBA/ntb3/77fNnz57N8OHDGT58OE8++WSb1KhpkiJ+7O2vcvhqzyEeu3QkXTqEMq5/D968dQLXvriGy55dyYvXnUJqUneny2yVBxZsZkuuZ/+zSo7rzP0XDGv0cVlZWcydO5cXXniByy+/nLfffhuAsLAwli9fzlNPPcXMmTNZt24d3bt356STTuKOO+5g9+7dvPzyy6xevRprLePGjePMM8/k5JNP9ujn0BG8iJ8qKa/mz2mZjO7XlUtGx3/bPiSmM2/fdirRncK5+sXVLN6c72CVvi0pKYmUlBQAxowZw+7duwGYMWMGACNGjGDYsGHExsYSHh5O//792bt3L5999hkXXXQRHTt2pFOnTlx88cWsWLHC4/XpCF7ET83+cCsHj1bxrxtSCQr6/jS7vt0jeeu2U/nRK19y22vr+OOFI7hqXD+HKm2dphxpt5Xw8PBvt4ODgykvL/9ee1BQ0PceExQURE1NDdbadqlPR/AifmhzbglzVmVz9fgEhvfp0uBjuncMY+7N4zhjYDS/eXcTT32U1W7BE+gmTpzIe++9x9GjRykrK+Pdd9/ljDPO8Pj76AhexM9Ya7n/v5vpFhnGr84ZfMLHRoaF8M/rxnL3Wxt54qNtFB2p4IEZwwkOCpyTiJwwevRorr/+elJTUwG46aabPN7/DmC84X/ssWPHWl3wQ8Qz3l6Xw6/e3MCjl4zk8lP6Nuk5dXWWRxZl8tzynUwbHsMTV6QQERrcxpW2XEZGBkOHDnW6jHbV0Gc2xqyz1o493nN0BC/iR0rKq3k4LYOT+3Xl0jHxjT/BLSjIcO/0oURHhfPHDzIoLlvDC9eNpXNE81YvFO+iPngRP/LEkm0cKKviDzOH/8/AalPcdEZ/nrwihXXZB7niuVUUHg6M5Xj9lQJexE9syT3Mqyt388Nx/Y47sNoUF57ch5euP4XsA2Vc/MwX7Cw64sEqPccbupfbS0s/qwJexA9Ya7l/fjpdI8O489wTD6w2xcRB0cy9eTxHq2q59NmVbNh7qPVFelBERAQHDhwIiJD/Zj34iIiIZj9XffAifuDdr/fx5e6DPHLJCLpGhnnkNUf17cpbt07g2pfWcOULq3jm6jGcOSjaI6/dWvHx8eTk5FBUVOR0Ke3imys6NZdm0Yj4uMMV1Zz1+DLiu3XgndtObVHf+4kUHq7gupe/JKuglMcvG8WFJ/fx6OtLyzU2i0ZdNCI+7sklWRwoq+TBmcM8Hu4AvTpH8MaPxzM2sRu/eGM9/1yx0+PvIW1DAS/iwzLzD/Ovlbu5KrUfI+O7ttn7dI4I5ZUfpTJ9RAx//CCDhxdmUFfn/Ld/OTH1wYv4KGstv39vM50jQrhrSusHVhsTERrM364cTY+Om3lu+U6KSit55NKRhAbrONFbKeBFfNR/1+eyZncxD1/suYHVxgQHGR6cOYzoqHBmL9lG8dEq/vHD0USGKUq8kf7rFfFBpRXVPLQwg1F9u3LF2KYtR+Apxhj+7+yBPHzxCJZvK+JKXQbQayngRXzQkx9lsf9IJX9oo4HVprgytR/PXD2GTF0G0Gsp4EV8zNb8Ul75YjezTmnbgdWmmDIshjk3jmO/LgPolRTwIj7EWtc1VqMiQvh1OwysNkVqUnfevPVUAC57diWrdx5wuCL5RqMBb4x5yRhTaIxJr9eWYoxZZYxZb4xZa4xJrXffvcaY7caYrcaYKW1VuEggmr8hl9W7ivn1lCF069g+A6tNMTgmynUZwKhwrnlpDYvSdRlAb9CUI/hXgKnHtD0KPGCtTQF+7/4ZY0wyMAsY5n7OP4wx3ruotIgPKa2o5qEPMhgZ34UrmrjOe3uK7xbJW7eeSnJsZ37y+jpeX53tdEkBr9GAt9YuB4qPbQY6u7e7ALnu7ZnAf6y1ldbaXcB2IBURabW/Ls2i6EglD8703isude8Yxr9vHseZg6K57910XQbQYS3tg/8F8JgxZi/wOHCvu70PsLfe43Lcbf/DGHOLu3tnbaAsGCTSUtsKSnnp893MOqUvKX27Ol3OCUWGhfD8tWO5ZHQ8T3y0jd++l06tznp1REsD/jbgDmttX+AO4EV3e0OHFQ3+Zq21z1trx1prx0ZHe8cKdSLeqP7A6l1ThjhdTpOEBgfx+GUjufXMk3h99R5uf/0rKqprnS4r4LQ04K8D3nFvv8l33TA5QP3OwXi+674RkRZYsDGPVTuLufPcwXT3ooHVxhhjuGfaEH53fjKLNudz3UtrKCmvdrqsgNLSgM8FznRvnwVkubfnA7OMMeHGmCRgILCmdSWKBK4jlTU89MEWhvfpzJWp/Zwup0VuPD2Jp2al8NWeg1zx3EpdBrAdNbqAhDFmLjAJ6GmMyQHuB24GnjLGhAAVwC0A1trNxph5wBagBrjdWqvvZSIt9NelWRQcruTZq8d47cBqU8xM6UO3yDBufW0dM//+Ob+ZPpTzR8ZijO9+Jl+gC36IeKmsglKmPbWCS0bH88ilI50uxyPS95Vw55sbyMwvJaVvV3573lDGJnZ3uiyfpQt+iPgg1zVWNxMZFsyvp3rHGaueMLxPFz74vzN49NKR5B4q59JnV3Lba+vIPlDmdGl+SQEv4oXe35jHFzsOcNfUIfToFO50OR4VHGS4fGxfPr1rEndMHsSybUVMnr2MBxds4dBRrUrpSQp4P1JZU6tlW/1AWWUND32QwfA+nbnKRwdWmyIyLISfTx7Ip3dO4pLR8bzyxS4mPvoJ/1yxk8qawBi623eonO2FR9rs9RXwfuThhZmM/9NSnlu2QyeW+LC/fpxF/uEKHpjhvWeselKvzhH8+ZKRLPz5GaT068YfP8jgnNnL+WBjnl+eBVtXZ1mRVcTNr67ljEc+5s9pGW32XroMi5+oqa1jwYZcwkOCeDgtk6WZhfzlslH07R7pdGnSDNsLS3lxxS4uHxvPmIRuTpfTrobEdObVG1JZtq2IP32Qwe3//orR/bpy33nJfrEvSsqreXtdDq+tymbn/jJ6dAzj1jNP4qpxbfctTQHvJ9bsLuZAmevyaWWVNTywYAvTnlrBAzOGcfHoPpqO5gPqD6zePdU3zlhtC2cOiub0AT15c+1e/rJkG5c88wXnjYzl7ilD6NfD9w5YMvIO8+rKbN77eh/l1bWc3K8rT1wxiukjYgkPadu1GBXwfiJtUz4RoUFMGhxNZFgI4/v34Jfz1vOrNzfwUUYBD100wqfOggxECzfl8/n2Azw4c5jfDaw2V3CQYVZqPy4YFcdzy3fy/PIdLNlcwHWnJvDTHwykS2So0yWeUFVNHWnpecxZmc3a7IOEhwQxMyWOayckMrxPl3arQ/Pg/UBdnWXcw0sZm9CNZ64e8217bZ3l+eU7mb1kK10jw3js0pFMGtzLwUrleMoqa5g8exndIsNY8LPTA6LvvTnySyr4y4dbeeurHLp0COX/zhrI1eMTCAvxrmHEvJJy/r16D3PX7GX/kUoSekRyzfgELh0T3yYXRm9sHryO4P3Auj0HKSqtZOrwmO+1BwcZbpt0EhMH9eSON9Zz/ctfcs34BO6dPoTIMP3qvcnfPt5OXkkFT191ssK9ATFdInjsslH86LQk/rQwgwff38KrK3dzz7QhTBkW42gXpLWWL3YcYM7KbJZkFFBnLWcN7sU1ExKYODDasWvmggLeL6RtyicsJIizhjR8dD4srgvzf3o6jy/eyj8/28Xn2/cz+4oUr192NlDsKDrCi5/t5NIx8YxJ0FmdJ5Ic15k5N6byqXsg9tbXvuKUxG7cd15yu/99Lq1wDZrOWZXNjqIyukWGctMZSVw9LsFrJjeoi8bH1dVZTnvkY4bFdeaf153S6OO/2L6fO9/cQEFpJT87awC3/2AAocHe9TU3kFhrufalNazfe4hP7pxEzwDve2+Omto65q3NYfaSrew/UsUFo+L49ZTBbR6uW/NLeXXlbt79eh9Hq2oZFd+FayYkcv7IWCJC2/cCduqi8XMbcg6RV1LBnec27XT2Uwf0JO0XE/l/8zfz5EdZfLK1iCcuH0X/6E5tXKk0ZFF6Piuy9vPAjGEK92YKCQ7iqnH9mJESx3PLdvDCip0sTs/nR6cl8pMfDKBLB88NxFbX1rEoPZ85q7JZs6uYsJAgZoyK45rxCYzy4m/COoL3cQ8vzOClz3ex9r5zmj2z4P2Nudz3bjqVNbXcd14yV4/r5xfTKWtq61iaWchb63Lo0TGMGaPiGNe/h9f1bR+tqmHyX5bRJTKMBT89jRB9k2qVvJJyHl+8jXe+zqFrh1B+fvZAfjg+oVXfUPNLKvj3mj3MXbOHotJK+nbvwNXjErh8bF+vuOh5Y0fwCngfZq1l4mOf0L9nJ/51Q8sufZtfUsFdb21gRdZ+Jg2O5tFLRtKrc4SHK20f+49U8saXe3l9VTa5JRX0igrnSGUNR6tq6RUVznkjY5kxKo6Uvl294j+yRxdl8o9Pd/DWrRO0oqIHpe8r4aEPMli58wBJPTtyz7QhnJvcu8m/c2stq3YWM2fVbhZvdg2anjkommsnJHDmoF5edaCggPdj6ftKOP9vn/HIJSO44pSWnw1XV2eZsyqbPy3MIDIsmD9dNIJpI2I9WGnbsdby1Z6DvLoym4Wb8qiutZw2oAfXjE9k8tBeVNdalmYWMH99Lp9uLaKqto5+3SOZMSqOGSlxDOod5UjdO4uOMOXJ5VwwKo7Zl6c4UoM/s9bycWYhf1qYwY6iMlKTuvPb84YyMr7rcZ9zpLKGd79yDZpuKzhClw6hXD42nqvHJ5DQo2P7Fd8MCng/9tjiTJ5dtpMv75vskZOYthce4Y431rNpXwkXj+7D/5sxjM4R3nlCSXlVLf9dv49XV2azJe8wUeEhXDLG9Y9xQK+GxxNKyqtZvDmfBRty+Xz7fuosDImJ4oJRccwYFdduMx++HVjdc4iP75xEdJT63ttKTW0dc7/cy5NLtnGgrIoLU+K4c8pg4rt997vOKihlzqps3vlqH0cqaxjRpwvXTEjggpFxdAhr30HT5lLA+ylrLWf/ZRmxXSN4/abxHnvd6to6/rY0i6c/2U5slw7MvnwU4/r38Njrt9au/WXMWZnNW+v2criihiExUVwzIYELU/rQMbzpcwaKSiv5YGMu8zfk8tWeQwCc3K8rM0bFcd7IWHpFtV031aL0PG597SvuvyCZH52W1GbvI98prajmmU938OJnu7C4LiM4LK4zr63KZtXOYsKCgzh/ZCzXTEjwmi68plDA+6mt+aVMeXI5f7xwOFePT/D466/LPsgv561nT/FRbjmjP788d1Cbr5txPLV1rq/br67czYqs/YQEGaYOj+HaCYmcktit1f8Y9xYfZcHGXOavzyUzv5QgAxNO6sGMUXFMHRbr0dPij1bVcM7s5URFhPD+z07XwGo723eonMcXb+Xdr/cB0KdrB344vh9XjO3rk8tDKOD91BNLtvHXj7NY/Zuz2+xos6yyhj9+kMHcNXsYEhPFk7NSGBLTuU3eqyEHjlTyxtq9vL5qD/sOldO7czhXpSZwZWrfNhsIziooZf4G15F99oGjhAUHMXFQNDNS4pg8tFerzwB+bHEmf/9kB/N+PIHUJA2sOmVL7mH2H6nktAE9vWrQtLkU8H5qyhPL6RIZyrwfT2jz91qaUcDdb2/kcHkNd00ZzI2nJ7XZ6dfWWtbvPcScldm8vzGPqto6JvTvwbUTEpic3LvdTsqy1rIxp4T5G3J5f2MuBYcriQwL5pzk3swYFccZA6ObvQ7Krv1lTHliOeePjGX2FSltU7gEFAW8H9peeITJs5e1ax/ugSOV3PvOJj7cUsD4/t15/LJR3xuoaq2K6lrmb8hlzspsNu0roVN4CBeP7sM14xMY6NBMl2/U1lnW7Cpm/oZc0tLzOHS0mq6RoUwbHsMFo+IYl9T4HHtrLde//CVfZR9k6Z1ntmkfvwQOBbwfevrjLB7/cBsr7z2L2C4d2u19rbW8uS6HB+ZvJsgYHpg5jItObt1a89kHynhtVTbz1uZQUl7NwF6duHZCAheNjqdTMwZN20tVTR2fbS/iv+tzWbKl4Ns59uePdE27HBXfpcH9sXhzPj+es47fnZ/MjadrYFU8QwHvh8776wrCQ4J45yenOfL+e4uP8st56/ly90Gmj4jhoQtHNOusvto6y7Jthby6Mptl24oIMoapw2K4ZkIC45K6+8wMhvKqWj7KKGD+hlyWuefYJ/SI5IKR359jX15Vy+TZyzSwKh6ngPcz2QfKOPOxT7lv+lBuntjfsTpq6yzPLd/BE0u20S0yjEebsNb8wbIq5q3dy2urs9lbXE6vqHCuTO3HVeP60dtHz579Rkl5NYvT85m/IZcvdnx/jn3h4Qr+tTKbN24Z71VTTsX3abExP5OWng/wP2u/t7fgIMNPJg1g4sDo7601/5vpQ//n5JANew/x6spsFmzMpaqmjnFJ3bl7qmsdb39ZybJLh1AuP6Uvl5/Sl8LSChZuzGP+hlweW7wVgAtT4hTu0u50BO9jZv79c6y1zP/p6U6X8q2K6loeW7yVFz/bRf+eHZl9RQpDYqJ4f2Mec1buZkNOCZFhwe5B00QGxzg7aNqe9hYfZUXWfs4b4dn59CKgI3i/su9QORv2HuLXU5u2NHB7iQgN5nfnJ3PWkF7c+eYGLnnmC6IiQjh0tJqTojt+e+HvKC9d9qAt9e0eyVXjWr5OkEhrKOB9yCJ398y04d65ENhpA3qy6BcTeXRRJiXl1VyV2o8JJ/XwmUFTEX+jgPchaZvyGBITRVJP71zZDlx90Q9dNMLpMkQE8I8RrgBQcLiCtdkHme4jy/iKiPMU8D5i8WZX98z0Ec7OnhER36GA9xELN+UxoFcnBvQKnBkoItI6CngfsP9IJWt2FTPd4bnvIuJbFPA+4MPNBdRZmOqls2dExDsp4H1AWnoeiT0iGRqr7hkRaToFvJc7WFbFFzsOMG1ErOaTi0izKOC93JKMAmrrLNPU/y4izaSA93KL0vPp07UDI/p0cboUEfExjQa8MeYlY0yhMSb9mPafGWO2GmM2G2Merdd+rzFmu/u+KW1RdKA4XFHNiqwipg2PUfeMiDRbU5YqeAV4Gnj1mwZjzA+AmcBIa22lMaaXuz0ZmAUMA+KAj4wxg6y1tZ4uPBAszSigutYyTWevikgLNHoEb61dDhQf03wb8GdrbaX7MYXu9pnAf6y1ldbaXcB2INWD9QaUtE35xHSO4OS+XZ0uRUR8UEv74AcBZxhjVhtjlhljTnG39wH21ntcjrvtfxhjbjHGrDXGrC0qKmphGf6rrLKGZduKmDo8hqBGLugsItKQlgZ8CNANGA/cBcwzrk7ihpKowSuKWGuft9aOtdaOjY6ObmEZ/uuTrYVU1tRp9oyItFhLAz4HeMe6rAHqgJ7u9r71HhcP5LauxMCUtimfnp3CGZvY3elSRMRHtTTg3wPOAjDGDALCgP3AfGCWMSbcGJMEDATWeKDOgFJeVcvHmYVMGdabYHXPiEgLNTqLxhgzF5gE9DTG5AD3Ay8BL7mnTlYB11nXxV03G2PmAVuAGuB2zaBpvmXbiiivrtXa7yLSKo0GvLX2yuPcdfVxHv8Q8FBrigp0ael5dIsMZVySumdEpOV0JquXqaypZWlGIecmxxASrF+PiLScEsTLfJa1nyOVNUzTlZtEpJUU8F5m4aZ8oiJCOPWknk6XIiI+TgHvRapq6liyJZ9zknsTFqJfjYi0jlLEi6zceYDDFTVM05WbRMQDFPBeJG1THh3DgjljoLpnRKT1FPBeoqa2jg+3FHD20N5EhAY7XY6I+AEFvJdYs6uY4rIqrT0jIh6jgPcSaen5dAgNZtLgXk6XIiJ+QgHvBWrrLIs25zNpcDQdwtQ9IyKeoYD3AuuyD1JUWqkrN4mIRyngvUBaeh5hIUGcNUTdMyLiOQp4h9XVWRal5zNxYDSdwptyiVwRkaZRwDtsQ84h8koqmK61Z0TEwxTwDktLzyc02HD20N5OlyIifkYB7yBrLQs35XHagJ506RDqdDki4mcU8A7anHuYnIPlTNfaMyLSBhTwDlq4KY/gIMM5yeqeERHPU8A7xFpLWno+E/r3oFvHMKfLERE/pIB3yNaCUnbtL9OVm0SkzSjgHbJwUz7GwLnJCngRaRsKeIcsSs8jNbE70VHhTpciIn5KAe+A7YWlbCs4oqWBRaRNKeAdkLYpH4Cpmh4pIm1IAe+AtPR8xiR0I6ZLhNOliIgfU8C3s+wDZWzJO6zuGRFpcwr4dpaW/k33jAJeRNqWAr6dpW3KY2R8F+K7RTpdioj4OQV8O8o5eJQNOSVM0+CqiLQDBXw7WuTunlH/u4i0BwV8O0pLz2dobGcSe3Z0uhQRCQAK+HaSX1LBuuyDTNfRu4i0EwV8O1m82d09M0L97yLSPhTw7WThpjwG9urEgF6dnC5FRAKEAr4dFJVW8uXuYh29i0i7UsC3gw+35FNnNXtGRNqXAr4dLErPJ6lnR4bERDldiogEEAV8GztYVsUXOw4wbXgMxhinyxGRANJowBtjXjLGFBpj0hu4705jjDXG9KzXdq8xZrsxZqsxZoqnC/Y1S7YUUFtndfaqiLS7phzBvwJMPbbRGNMXOAfYU68tGZgFDHM/5x/GmGCPVOqj0tLziO/WgeF9OjtdiogEmEYD3lq7HChu4K4ngF8Dtl7bTOA/1tpKa+0uYDuQ6olCfVFJeTWfbd+v7hkRcUSL+uCNMTOAfdbaDcfc1QfYW+/nHHdbQ69xizFmrTFmbVFRUUvK8HpLMwqorrWaHikijmh2wBtjIoH7gN83dHcDbbaBNqy1z1trx1prx0ZHRze3DJ+Qlp5PbJcIUuK7Ol2KiASglhzBnwQkARuMMbuBeOArY0wMriP2vvUeGw/ktrZIX3SksoZl24qYMiyGoCB1z4hI+2t2wFtrN1lre1lrE621ibhCfbS1Nh+YD8wyxoQbY5KAgcAaj1bsIz7JLKSqpo7p6p4REYc0ZZrkXGAlMNgYk2OMufF4j7XWbgbmAVuARcDt1tpaTxXrS9LS8+jZKZwxCd2cLkVEAlRIYw+w1l7ZyP2Jx/z8EPBQ68rybeVVtXySWcQlY/oQrO4ZEXGIzmRtA8u2FVJeXct0ndwkIg5SwLeBhZvy6RYZSmpSd6dLEZEApoD3sIrqWj7OLGTKsBhCgrV7RcQ5SiAP+yxrP0cqa3Ryk4g4TgHvYQvT8+gcEcKE/j2cLkVEApwC3oOqaur4aEsB5yTHEBaiXSsizlIKedAXO/ZzuKJGV24SEa+ggPegRen5dAoP4fSBPRt/sIhIG1PAe0hNbR2LN+dz9tBeRIQG9BL4IuIlFPAesnpXMQePVqt7RkS8hgLeQ9LS8+gQGsyZg3o5XYqICKCA94jaOsui9AJ+MCSaDmHqnhER76CA94Avdxez/0ilLqwtIl5FAd9K1lqe/Ggb3SJDOWuIumdExHso4Ftp8eZ8Vu0s5pfnDqZjeKOrL4uItBsFfCtUVNfyxw8yGNw7iitP6dv4E0RE2pECvhVe/GwXOQfL+f0FyVo5UkS8jlKphQoPV/D3T7ZzTnJvThugM1dFxPso4Fvo0cVbqa6t477pQ50uRUSkQQr4FtiYc4i31uVww2lJJPbs6HQ5IiINUsA3k7WWBxdsoWenMH561gCnyxEROS4FfDMt2JjH2uyD3DVlMFERoU6XIyJyXAr4ZiivquXhhRkMi+vMpWM0LVJEvJsCvhmeW76DvJIK7r9gGMFBxulyREROSAHfRLmHynl22Q7OGxFLalJ3p8sREWmUAr6JHlmUSZ2Fe6YNcboUEZEmUcA3wbrsg/x3fS63nNGfvt0jnS5HRKRJFPCNqKuzPLhgM72iwrlt0klOlyMi0mQK+Ea88/U+NuSUcM+0IVotUkR8igL+BMoqa3h0USaj+nblwpQ+TpcjItIsCvgT+Men2yksreT+C5IJ0rRIEfExCvjj2Ft8lBdW7OLClDhG9+vmdDkiIs2mgD+Oh9MyCDaGuzUtUkR8lAK+Aat2HmDhpnxuPfMkYrt0cLocEZEWUcAfo7bOtVpkn64duGVif6fLERFpMQX8Meat3cuWvMPcM20IHcKCnS5HRKTFFPD1HK6o5vHFWzklsRvnj4x1uhwRkVbRmTv1PP3xdoqPVvHK+akYo2mRIuLbGj2CN8a8ZIwpNMak12t7zBiTaYzZaIx51xjTtd599xpjthtjthpjprRR3R63a38ZL3++i0tHxzMivovT5YiItFpTumheAaYe07YEGG6tHQlsA+4FMMYkA7OAYe7n/MMY4xMd2Q99kEFYcBB3TR3sdCkiIh7RaMBba5cDxce0fWitrXH/uAqId2/PBP5jra201u4CtgOpHqy3TazIKuKjjAJuP2sAvaIinC5HRMQjPDHIegOQ5t7uA+ytd1+Ou+1/GGNuMcasNcasLSoq8kAZLVNTW8cf3t9Cv+6R3HBakmN1iIh4WqsC3hhzH1ADvP5NUwMPsw0911r7vLV2rLV2bHR0dGvKaJV/r9nDtoIj/Gb6UCJCfaI3SUSkSVo8i8YYcx1wPnC2tfabEM8B6l+NOh7IbXl5bevQ0SpmL9nGhP49mDKst9PliIh4VIuO4I0xU4G7gRnW2qP17poPzDLGhBtjkoCBwJrWl9k2nvwoi8Pl1fz+gmRNixQRv9PoEbwxZi4wCehpjMkB7sc1ayYcWOIOxlXW2luttZuNMfOALbi6bm631ta2VfGtsb2wlDmrspmV2o+hsZ2dLkdExOMaDXhr7ZUNNL94gsc/BDzUmqLawx/ezyAyLJhfnTPI6VJERNpEQC5V8ElmIcu2FfHzswfSo1O40+WIiLSJgAv4qhrXtMj+PTty7YREp8sREWkzARfwr67czc79Zfz2/KGEhQTcxxeRABJQCXfgSCVPLc1i4qBofjC4l9PliIi0qYAK+NlLtnG0qpbfnTdU0yJFxO8FTMBn5B1m7po9XDM+gYG9o5wuR0SkzQVEwFvrugxf5w6h/GLyQKfLERFpFwER8Is3F7By5wHumDyIrpFhTpcjItIu/D7gK2tq+dPCDAb17sQPx/VzuhwRkXbj9wH/0me72VN8lN+dn0xIsN9/XBGRb/l14hWWVvD0x1lMHtqLMwY6tySxiIgT/DrgH1+8laraOu47L9npUkRE2p3fBnz6vhLeXJfD9acmktSzo9PliIi0O78MeGstDyzYTPfIMH52tqZFikhg8suAf39jHl/uPsivzh1M54hQp8sREXGE3wV8RXUtf07LZGhsZ644pW/jTxAR8VN+F/DPL9/JvkPl3H9BMsFBWm9GRAKXXwV8fkkFz3y6g2nDYxjfv4fT5YiIOMqvAv6RRZnUWstvpg91uhQREcf5TcB/tecg7369j5tOT6Jv90inyxERcZxfBHxdneWBBVuIjgrnJz8Y4HQ5IiJewS8C/r31+9iw9xB3Tx1Cp/AQp8sREfEKPh/wZZU1PLIok1HxXbj45D5OlyMi4jV8PuCfXbaDgsOV/P6CZII0LVJE5Fs+HfA5B4/y/PKdzBgVx5iE7k6XIyLiVXw64Ktq6hjXvwf3TBvidCkiIl7Hp0ck+0d34tUbUp0uQ0TEK/n0EbyIiByfAl5ExE8p4EVE/JQCXkTETyngRUT8lAJeRMRPKeBFRPyUAl5ExE8Za63TNWCMKQKyna6jlXoC+50uwotof3yf9sd3tC++rzX7I8FaG328O70i4P2BMWattXas03V4C+2P79P++I72xfe15f5QF42IiJ9SwIuI+CkFvOc873QBXkb74/u0P76jffF9bbY/1AcvIuKndAQvIuKnFPAiIn5KAX8cxpi+xphPjDEZxpjNxpifu9u7G2OWGGOy3H92q/ece40x240xW40xU+q1jzHGbHLf91djjM9ePNYYE2yM+doY877754DdH8aYrsaYt4wxme6/JxMCdX8YY+5w/ztJN8bMNcZEBNK+MMa8ZIwpNMak12vz2Oc3xoQbY95wt682xiQ2qTBrrW4N3IBYYLR7OwrYBiQDjwL3uNvvAR5xbycDG4BwIAnYAQS771sDTAAMkAZMc/rztWK//BL4N/C+++eA3R/Av4Cb3NthQNdA3B9AH2AX0MH98zzg+kDaF8BEYDSQXq/NY58f+AnwrHt7FvBGk+pyesf4yg34L3AOsBWIdbfFAlvd2/cC99Z7/GL3LyoWyKzXfiXwnNOfp4X7IB5YCpxVL+ADcn8And2hZo5pD7j94Q74vUB3XJcBfR84N9D2BZB4TMB77PN/8xj3dgiuM19NYzWpi6YJ3F+HTgZWA72ttXkA7j97uR/2zV/yb+S42/q4t49t90VPAr8G6uq1Ber+6A8UAS+7u6z+aYzpSADuD2vtPuBxYA+QB5RYaz8kAPfFMTz5+b99jrW2BigBejRWgAK+EcaYTsDbwC+stYdP9NAG2uwJ2n2KMeZ8oNBau66pT2mgzW/2B66jqNHAM9bak4EyXF/Dj8dv94e7b3kmru6GOKCjMebqEz2lgTa/2BdN1JLP36J9o4A/AWNMKK5wf91a+467ucAYE+u+PxYodLfnAH3rPT0eyHW3xzfQ7mtOA2YYY3YD/wHOMsa8RuDujxwgx1q72v3zW7gCPxD3x2Rgl7W2yFpbDbwDnEpg7ov6PPn5v32OMSYE6AIUN1aAAv443KPXLwIZ1trZ9e6aD1zn3r4OV9/8N+2z3KPdScBAYI37q1mpMWa8+zWvrfccn2GtvddaG2+tTcQ1yPOxtfZqAnd/5AN7jTGD3U1nA1sIzP2xBxhvjIl0f4azgQwCc1/U58nPX/+1LsX176/xbzdOD0x46w04HddXoI3AevdtOq5+r6VAlvvP7vWecx+uEfGt1Bv9B8YC6e77nqYJgyPefAMm8d0ga8DuDyAFWOv+O/Ie0C1Q9wfwAJDp/hxzcM0QCZh9AczFNf5Qjeto+0ZPfn4gAngT2I5rpk3/ptSlpQpERPyUumhERPyUAl5ExE8p4EVE/JQCXkTETyngRUT8lAJefJJ7JcefuLfjjDFvteF7pRhjprfV64u0FQW8+KquuFbYw1qba629tA3fKwXXORAiPkXz4MUnGWP+g2v9k624TiQZaq0dboy5HrgQCAaGA3/BtZTvNUAlMN1aW2yMOQn4OxANHAVuttZmGmMuA+4HanEt6DQZ18klHYB9wMO4VpF80t1WDvzIWru1Ge/9Ka4T51JxrUp5g7V2jef3kgQ8p88A0023ltyotzTrMdvX4wrkKFzhXQLc6r7vCVyLxoHrzMKB7u1xuE79BtgE9HFvd633mk/Xe+/OQIh7ezLwdjPf+1PgBff2ROotMaubbp68hXjqPwoRL/KJtbYU17oeJcACd/smYKR7hdBTgTfrXTAo3P3n58Arxph5uBbNakgX4F/GmIG4lrMIbep713vcXABr7XJjTGdjTFdr7aGWfVyRhingxR9V1tuuq/dzHa6/80HAIWttyrFPtNbeaowZB5wHrDfG/M9jgD/gCvKL3NcK+LQZ7/3tWx371sf/OCIto0FW8VWluLpCms261vXf5e5vx7iMcm+fZK1dba39Pa6r5vRt4L264OqPB1e3TEtc4X6/03FdIKOkha8jclwKePFJ1toDwOfuixw/1oKX+CFwozFmA7AZ14AtwGPuix6nA8txXTvzEyDZGLPeGHMFrmttPmyM+RzXgGpLHDTGfAE8i2vlQRGP0ywakXbmnkVzp7V2rdO1iH/TEbyIiJ/SEbyIiJ/SEbyIiJ9SwIuI+CkFvIiIn1LAi4j4KQW8iIif+v+mx7pkOxUgbAAAAABJRU5ErkJggg==\n",
      "text/plain": [
       "<Figure size 432x288 with 1 Axes>"
      ]
     },
     "metadata": {
      "needs_background": "light"
     },
     "output_type": "display_data"
    }
   ],
   "source": [
    "yaml_df.nmol = yaml_df['nmol'].apply(lambda x: int(x[0]))\n",
    "yaml_df = yaml_df.set_index('timestamp')\n",
    "yaml_df.plot.line(y='nmol')"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b6e75b98",
   "metadata": {},
   "source": [
    "From this, we extract the time sequence of the number of particles in the system. \n",
    "\n",
    "#### Challenge 1 - testing equilibration\n",
    "Does the system look equilibrated to you? How can we assess this quantitiatively? First, try increasing the length of the simulation to establish whether the system has equilibrated. Then, can you apply the algorithms in [this paper](https://pubs.acs.org/doi/10.1021/acs.jctc.5b00784)? Hint - check out the `timeseries module` in their [python package](https://pymbar.readthedocs.io/en/master/).\n",
    "\n",
    "#### Challenge 2 - the effect of different parameters\n",
    "\n",
    "What happens as you systematically vary the temperature and activity (chemical potential)?  \n",
    "You can  explore histograms once you have produced the time sequence of adsorbed particles with `plot.hist()` method of the resulting dataframe."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "dbbe46c7",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Your code here!"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "dlmontepython",
   "language": "python",
   "name": "dlmontepython"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.2"
  },
  "toc": {
   "base_numbering": 1,
   "nav_menu": {},
   "number_sections": true,
   "sideBar": true,
   "skip_h1_title": true,
   "title_cell": "Table of Contents",
   "title_sidebar": "Contents",
   "toc_cell": false,
   "toc_position": {},
   "toc_section_display": true,
   "toc_window_display": false
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
