{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "f4ff8607",
   "metadata": {},
   "source": [
    "# Tutorial 7: Sweeping through variuables with `simtask`\n",
    "Original authors: Joe Manning (joseph.manning{at}manchester.ac.uk) and Tom L. Underwood (tlu20{at}bath.ac.uk)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "028fe97b",
   "metadata": {},
   "source": [
    "## Introduction\n",
    "\n",
    "Tutorial 6 demonstrated how `simtask` could be used to calculate the value and uncertainty of an observable automatically at a given set of thermodynamic conditions: the energy and density was calculated at a specified chemical potential and temperature. We will now go further, automating the calculation of the energy and density at *many* temperatures.\n",
    "The relevant `simtask` class is `MeasurementSweep`, which performs sets of simulations similar to that performed by `Measurement` at a list of different *control parameters*.\n",
    "\n",
    "Similarly to `Measurement`, to invoke the task corresponding to a `MeasurementSweep` object `sweep` the command is\n",
    "\n",
    "```python\n",
    "sweep.run()\n",
    "```\n",
    "\n",
    "However, first one must create the object `sweep`, parameterised appropriately for the task at hand."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "df9a9f9f",
   "metadata": {},
   "source": [
    "## `Simtask` setup\n",
    "\n",
    "We'll set up simtask as we did before:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "id": "65ab7b2a",
   "metadata": {
    "ExecuteTime": {
     "end_time": "2022-08-11T15:12:17.259053Z",
     "start_time": "2022-08-11T15:12:08.931910Z"
    }
   },
   "outputs": [],
   "source": [
    "import dlmontepython.simtask.dlmonteinterface as interface\n",
    "import dlmontepython.simtask.measurement as measurement\n",
    "import dlmontepython.simtask.analysis as analysis\n",
    "import dlmontepython.simtask.task as task\n",
    "\n",
    "# The line below creates a DL_MONTE-specific interface. Note that the interface \n",
    "# must know the location of the DL_MONTE executable - which is specified as the \n",
    "# argument to the DLMonteInterface constructor.\n",
    "# You will have to adapt the location of the executable to suit your local system.\n",
    "\n",
    "interface = interface.DLMonteInterface(\"DLMONTE-SRL.X\")\n",
    "\n",
    "energy_obs = task.Observable( (\"energy\",) )\n",
    "nmol_obs = task.Observable( (\"nmol\",1) )\n",
    "observables = [ energy_obs, nmol_obs ]\n",
    "\n",
    "precisions = { nmol_obs : 0.2 }\n",
    "\n",
    "m_template = measurement.Measurement(interface, observables, maxsims=20, \n",
    "                                     precisions=precisions, outputdir='./fixedprecision')"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "0986ff46",
   "metadata": {},
   "source": [
    "### Defining the `MeasurementSweep`\n",
    "New we have a template `Measurement` object invoked, we'll create a list of chemical potentials and create a `MeasurementSweep` to run through them."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "id": "7de65b76",
   "metadata": {
    "ExecuteTime": {
     "end_time": "2022-08-11T15:21:26.187717Z",
     "start_time": "2022-08-11T15:21:26.179533Z"
    }
   },
   "outputs": [],
   "source": [
    "molchempots = [1e-5, 1e-4, 1e-3, 1e-2, 1e-1, 1, 1e1, 1e2, 1e3]\n",
    "\n",
    "sweep = measurement.MeasurementSweep(param=\"molchempot\", paramvalues=molchempots, \n",
    "                                     measurement_template=m_template, outputdir=\"isotherm_simulation\")\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "568caf7f",
   "metadata": {},
   "source": [
    "## Running the isotherm\n",
    "\n",
    "As mentioned above, the task is invoked via the command\n",
    "\n",
    "```python\n",
    "sweep.run()\n",
    "```\n",
    "\n",
    "#### Troubleshooting\n",
    "\n",
    "If you encounter a problem with running the next block of code, one possible reason is that the path to the `DL_MONTE` executable used to initialise the `DLMonteInterface` object above does not exist, or is wrong.\n",
    "\n",
    "Also, you may encounter issues running this next cell on windows, so I've provided a python script for you to run the same simulation on Linux as required and continue on below.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "f135f53b",
   "metadata": {
    "ExecuteTime": {
     "end_time": "2022-08-11T15:23:20.724738Z",
     "start_time": "2022-08-11T15:23:20.112779Z"
    },
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "import os\n",
    "import shutil\n",
    "\n",
    "os.chdir('./Tutorial_7_files/')\n",
    "if os.path.exists('isotherm_simulation'):\n",
    "    shutil.rmtree('isotherm_simulation')\n",
    "\n",
    "sweep.run()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "7de49d08",
   "metadata": {},
   "source": [
    "## Postprocessing and analysis\n",
    "Just like how the `Measurement` object created \"energy_converge.dat\" and \"nmol_1_converge.dat\" in Tutorial 6, the `MeasurementSweep` object creates summary files called \"energy_sweep.dat\" and \"nmol_1_sweep.dat\", which provide the idata as a complete isotherm."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "id": "264b7239",
   "metadata": {
    "ExecuteTime": {
     "end_time": "2022-08-12T08:36:54.369401Z",
     "start_time": "2022-08-12T08:36:54.115075Z"
    }
   },
   "outputs": [
    {
     "data": {
      "text/plain": [
       "Text(0, 0.5, 'Molecules adsorbed (n/uc)')"
      ]
     },
     "execution_count": 6,
     "metadata": {},
     "output_type": "execute_result"
    },
    {
     "data": {
      "image/png": "iVBORw0KGgoAAAANSUhEUgAAAYUAAAEKCAYAAAD9xUlFAAAAOXRFWHRTb2Z0d2FyZQBNYXRwbG90bGliIHZlcnNpb24zLjUuMSwgaHR0cHM6Ly9tYXRwbG90bGliLm9yZy/YYfK9AAAACXBIWXMAAAsTAAALEwEAmpwYAAAuLUlEQVR4nO3dd3yV9fn/8ddFCCSMhBVWGAEEZCgr4mitAxW1qGBrtVW/aFVqv1ZbN666W6r+bKut+qWtilq3gKuClkptnYCo7L1CkA1hJCHj+v1x7oQAgZyEnJyR9/PxyCPn/pz73Pd1bsK5zv2Z5u6IiIgANIh2ACIiEjuUFEREpJySgoiIlFNSEBGRckoKIiJSTklBRETKNYx2AIejTZs2npWVFe0wRETiyqxZsza5e0Zlz8V1UsjKymLmzJnRDkNEJK6Y2aqDPafqIxERKaekICIi5ZQURESkXFy3KVSmqKiInJwcCgoKoh1KRKSkpNCpUyeSk5OjHYqIJKCESwo5OTk0b96crKwszCza4dQqd2fz5s3k5OTQrVu3aIcjIgko4ZJCQUFBQiYEADOjdevWbNy4MdqhiEiUTJ69loenLiJ3Wz4dW6Ry8/DejByUWWvHT7ikACRkQiiTyO9NRA5t8uy13DZxDvlFJQCs3ZbPbRPnANRaYkjIpCAiEu8KikrIyy8ir6CI7flF5OUXc+/b88oTQpn8ohIenrpISUFEJJIOt5qmqKQ0+FAvJi8/+GAvCH247/2gP/jze4pLwz5X7rb8mrzFSkUsKZjZ08AIYIO79w/KHgbOAfYAy4DL3X1b8NxtwBVACXCdu0+NVGx15bXXXuORRx4hPz+f3bt3c+mll3L33Xfzxhtv8NBDD1FQUEBycjIPPvggw4cPj3a4IhKorJrmlje+4eucrfRpnx58eJd9kFf+ob57T8khz9GwgZGemkxa2U9KQzJbppKWkhyUNyQtJfRcevD8z56fxYYdhQccq2OL1Fp775G8U3gW+BPwXIWyD4Db3L3YzH4H3AbcamZ9gYuAfkBH4J9m1svdD31Va0GkGm0mTJjA448/zuTJk+nUqRM7d+7kySef5MUXX+Txxx/nzTffpH379ixZsoQTTzyRGTNm0Llz51p4RyJSUxt2FDBr5VbumDzngGqaPcWlPPPx3tkhzAg+tBuWf5B3b9Nsn+204MM99KFe9uEfei4luUG12whvP7vPPskKIDU5iZuH9z68N15BxJKCu39kZln7lb1fYfMz4IfB4/OAl929EFhhZkuBocCnkYoPItdok5eXxw033MCMGTPo1KkTAM2aNeN///d/6dOnDx9//DHt27cHoGfPnpx88slMmzaNyy677PDekIiEzd1ZtnEnM1ZuZebKrcxctYVVm3cf8jUG/OfWU0hLTaZZo4Y0aFC3HT/KPpcStffRT4FXgseZhJJEmZyg7ABmNgYYA9ClS5dDnuDet+cxPzfvoM/PXr2NPSX71tvlF5Vwy+vf8NIXqyt9Td+Oadx9Tr9DnnfSpEkce+yxdO/efZ/yl19+mcGDBx9wR9C4cWO2b99+yGOKyOEpLC5hTs52Zq7aysyVW5i5aivbdhcB0LppI4Z0bcklx3ZlSFZLfvH3L8ndfuAA2I4tUunUskldh76PkYMyazUJ7C8qScHM7gCKgb+XFVWym1f2WncfD4wHyM7OrnSfcO2fEKoqD9e8efMYOHDgAeVz585lwIABB5R//fXXjB49msmTJ/Puu++yYcMGrrnmGs4444zDikOkPtu2ew+zVm1lxsqtzFq1ha9ztpc33nZv05Qz+rYju2srsrNa0q1N032qcm4588iIV9PEqjpPCmY2mlAD9DB3L/tQzwEqfn3uBOQe7rmq+kb/nXH/Ym0lrfaZLVJ55WfH1/i8TZs2JT//wOOmp6dTWLhvI9Gnn35KXl4eJ510EklJSYwcOZKtW7dy0003KSmIhMndWbMlnxnBHcDMlVtYsmEnAMlJRv/MdEYf35XsrFYM6dqSNs0aH/J4dVFNE6vqNCmY2ZnArcBJ7l6x8u4t4EUze5RQQ3NP4ItIx3Pz8N4R+TZw9tlnc9FFF3H99dfTrl07CgsLee655xgxYgQXXnghN9xwAxkZGSxevJgrr7ySZ555hqSkpPLXP/DAA1xzzTWHFYNIIisuKWXBuh1BEtjCzJVby3vlNE9pyJCuLRk5KJPsri0Z0LkFKclJVRzxQJGupolVkeyS+hJwMtDGzHKAuwn1NmoMfBDcqn3m7le7+zwzexWYT6ha6Zq66HkUqW8DxxxzDPfccw/Dhw+npKSE4uJiLrnkErKzs7nrrrsYNmwYZkZ6ejpPPfUUJ554IhD6tjN27FjOOussBg8efNjvTyRR7CwsZvbqvVVBs1dvK+/ymdkilRN6tCY7K1QV1Ktt8zpvAE4ktrcGJ/5kZ2f7/iuvLViwgD59+kQposPz2GOPMWHCBI455hgGDhzI1VdfXel+8fweRfZXWbfw47q3Lr8DmLFyCwvW5VHq0MCgT4c0jgmqgbKzWtIhvfb66NcXZjbL3bMrfU5JIf7Uh/co9cPk2WsZO/EbCor2du4w9vYySU1OYnDXFgzp2opjsloysHMLmqdo2vjDdaikoGkuRKTO7CosZuG3eczLzWPe2jwmzs6hqGTfL6YOpKc25PkrjqVPhzSSk7QWWF1SUhCRiNi0s5D5uUECyN3O/Nw8VmzeRVnlRKumjQ5ICGXy8os5ulOLugtWyikpiMhhKesOOi93O/PX7U0C6/P2dr/u1DKVfh3TOG9gJv06ptEvM432aSl893cfVtotvDbn8pHqScik4O4Ju+5APLcBSfwrKill6YadzMvNC+4CQolgR0ExAEkNjCMymvGdHm3o2zGNvh3T6NchnfQmlbcDRKpbuNRcwiWFlJQUNm/eTOvWrRMuMZQtx5mSkhLtUKQe2L/+f/66PBat31E+Kjg1OYkjOzTnvIEd6dcxnb4d0ujdvnm1xgTU50FisSrheh8VFRWRk5NDQcGB85YkgpSUFDp16kRysnpgSPUcakbgzTsLg2qfvd/+V2zaW//fskky/Tqm06/s23/HNLq1aUaSxgPEpXrVJVVEDrT/jMAQms+/V7tmbNlVxLd5e79EZbYI1f+XJYGy+v9Eu/Ouz9QlVaSee3jqogPWBygudRav38k5AzruvQM4RP2/1A9KCiIJzt0r7eEDUFLq/P7CgXUbkMQ0jQoRSWAFwfogB6Oun7I/JQWRBLVmy25++NQnvDYrh+F925KavO9/d3X9lMqo+kgkAX20eCPXvTybklLnr/+TzWl920VsPXJJLEoKIgmktNR58t/LeOT9RfRq25ynLh1CtzZNgfq7PoBUj5KCSILIKyjixle/5oP56zl3QEfG/eAomjTSf3GpHv3FiCSAxet3cPXzs1i9ZTe/HtGXy7+TpXEFUiNKCiJx7p1vcrnl9W9o0qghL151HEO7tYp2SBLHlBRE4lRxSSnj3lvIX/+7giFdW/LExYNpl6Z5seTwKCmIxKGNOwr5xYtf8vmKLYw+vit3fL8vjRqqh7kcPiUFkTjz5eqt/PyFWWzPL+L3Fw5g1KBO0Q5JEoiSgkiccHde+Hw19709jw7pqUz8+VD6dkyLdliSYJQUROJAQVEJd0yayxtf5nBK7wz+cOEgTVwnEaGkIBLj1mzZzdUvzGJebh6/HNaTXw7rSQOtYyARErGWKTN72sw2mNncCmWtzOwDM1sS/G5Z4bnbzGypmS0ys+GRiksknvx78UZGPP5f1mzZzdOXZXP96b2UECSiwk4KZtbSzPqZWXczC+d1zwJn7lc2Fpjm7j2BacE2ZtYXuAjoF7zmCTMLf00/kQRTWuo8Pm0Jlz3zBR3SU3j72u9y6pHtoh2W1AOHrD4ys3TgGuDHQCNgI5ACtDOzz4An3P3Dyl7r7h+ZWdZ+xecBJwePJwDTgVuD8pfdvRBYYWZLgaHAp9V/SyLxbXt+ETe++hX/XLCBkQM78tvzjya1kb4jSd2oqk3hdeA54ER331bxCTMbAlxqZt3d/W9hnq+du68DcPd1ZtY2KM8EPquwX05QdgAzGwOMAejSpUuYpxWJD4u+3cHPnp9JztZ87jmnL6NP0HQVUrcOmRTc/fRDPDcLmFVLcVT2V1/p4tHuPh4YD6E1mmvp/CJR99bXudz6+jc0S2nIS2OO45gsTVchdS+sNgUzGxVUJZVttzCzkTU433oz6xAcowOwISjPATpX2K8TkFuD44vEnaKSUu57ez7XvTSb/plpvHvtd5UQJGrCbWi+2923l20EVUl31+B8bwGjg8ejgTcrlF9kZo3NrBvQE/iiBscXiSsbdhRw8V8/5+mPV3DZCVm8eNVxtNX8RRJF4Y5TqCx5VNVI/RKhRuU2ZpZDKImMA141syuA1cAFAO4+z8xeBeYDxcA17l4SZmwicWnWqi3879+/ZHt+EX+4cKAWwJGYEG5SmGlmjwJ/JlTXfy1VtCe4+48P8tSwg+z/IPBgmPGIxC1357lPV3H/O/PJbJnKs5cPpU8HTVchsSHcpHAtcBfwCqFG4fcJdVUVkWrI31PC7ZPmMGn2WoYd2ZZHLxxIeqqmq5DYEVZScPddBAPNRKRmVm/ezc9emMXCb/O4/rReXHvqERqdLDEnrKRgZh9SSRdRdz+11iMSiXO//2Axf5y2pHz7l8N6MrBzC3758mwAnr7sGE7p3fZgLxeJqnCrj26q8DgF+AGhBmER2c/1p/fis+WbAXjpquN47F9L+OmEGRzZPo3/u2QIXVo3iXKEIgcXbvXR/o3KH5vZvyMQj0jcmzx7LbNXb2NPSSl9fj2FwuJSzh+UyYOjjtJ0FRLzwq0+qjiSpgEwBGgfkYhE4tjk2Wu5beIc9pSUAlBYXEpyknFizzZKCBIXwq0+mkWoTcEIVRutAK6IVFAi8ep3UxaSX7TvEJuiEueR9xczarCWzZTYV9UAtA7uvs7du9VVQCLxqLTUmTh7Leu2F1T6fO62/DqOSKRmqrpTeDpYCGc6MAX4r7urgVmkglmrtnLf2/P4Omc7yUlGUcmB8zR2bJEahchEqq+qWVLPMrMUQtNVjAIeMbPVhBLEFHdfHfkQRWLTuu35jHtvIW9+lUu7tMY8+qMBmMPtk+fuU4WUmpzEzcN7RzFSkfBV2abg7gUESQAgmLDuLOBPZtbe3YdGNkSR2JK/p4TxHy3nqX8vo8SdX5xyBD8/uQdNG4f+O1kD45bXv2FPSSmZLVK5eXhvzWskcSPchuZy7r4CeILQkpmNaj8kkdjk7rzzzTrGvbeQtdvy+f5RHRh71pF0brXvuIMVm3aV9z5auy2fFZt2RSNckRox96rXqTGz84HfAW0J9UAywN09qrN4ZWdn+8yZM6MZgtQTc3K2c98785ixcit9O6Rx9zl9ObZ762iHJVIjZjbL3bMrey7cO4WHgHPcfUHthSUS+zbsKOCRqYt4bVYOrZo0Ytz5R3FBdmeSNGeRJKhwk8J6JQSpTwqLS3jm45X86V9LKSwu4aoTu/OLU48gLUUzmkpiq856Cq8Ak4HCskJ3nxiJoESixd15f/56fvOPBazavJvT+rTlju/3pVubptEOTaROhJsU0oDdwBkVyhxQUpCEsfDbPO5/Zz4fL91Mz7bNeP6KoZzYMyPaYYnUqXAnxLs80oGIRMuWXXv4/QeL+fvnq2ieksy95/bj4mO70DAp3CXMRRJHVdNc3Ak84e5bDvL8qUATd38nEsGJRFJRSSnPf7qKP/xzMbv2lHDpcV351Wm9aNlUPa2l/qrqTmEO8LaZFQBfAhsJrafQExgI/BP4TSQDFImE6Ys2cP8781m2cRcn9mzDXSP60qtd82iHJRJ1VU1z8Sbwppn1BL4DdADygBeAMe6uWb4krizbuJMH3pnPh4s2ktW6CX8bnc2pR7bFTF1MRSD8NoUlwJIqdxSJUdvzi3hs2hImfLKS1OQk7ji7D6NPyKJRQ7UbiFRU7WkuROJJSanz8ozV/L/3F7N19x4uOqYzN57RmzbNGkc7NJGYFJWkYGbXA1cS6tY6B7gcaAK8AmQBK4EfufvWaMQnieGTZZu47+35LPx2B0O7teLXI/rSPzM92mGJxLQ6TwpmlglcB/R193wzexW4COgLTHP3cWY2FhgL3FrX8Un8W715N7/5xwKmzPuWzBapPHHxYM7q317tBiJhqKpL6uOEvs1Xyt2vO4zzpppZEaE7hFzgNkLrNgBMILSwj5KChG1nYTFPfLiUv/5nBQ2TjJvO6MWVJ3YnJVlrI4uEq6o7hbIpSL9D6Jv8K8H2BYTWba42d19rZo8Aq4F84H13f9/M2rn7umCfdWbWtibHl/rjZ8/NZOr89eXbZauenT8ok1vOPJL26SlRjE4kPlXVJXUCgJldBpzi7kXB9lPA+zU5YbC853lAN2Ab8JqZXVKN148BxgB06dKlJiFIApg8ey0fLdm0T1lxqfOr03ryq9N6RSkqkfgXbn+8jkDFkT3NgrKaOA1Y4e4bgyQzETgBWG9mHQCC3xsqe7G7j3f3bHfPzsjQvDT11cNTF+2z5CWAO7w2MydKEYkkhnAbmscBs83sw2D7JOCeGp5zNXCcmTUhVH00jFA11S5gdHCu0cCbNTy+1AO52yofN3mwchEJT7iD154xs/eAY4Oise7+bU1O6O6fm9nrhKbNKAZmA+MJ3X28amZXEEocF9Tk+FI/pKUmsz2/6IDyji1SoxCNSOIIKylYqC/faUB3d7/PzLqY2VB3/6ImJ3X3u4G79ysuJHTXIHJI6/MKKNhTHFoTtkJ5wwbGzcN7RysskYQQbpvCE8DxwI+D7R3AnyMSkUgV7nt7Pm7G7Wf3IbNFKgZktkjlkQsGMHJQZrTDE4lr4bYpHOvug81sNoC7bzUzzS8sde7DRRt4d846bjy9F1d9rztXfa97tEMSSSjh3ikUmVkSwd26mWUApRGLSqQS+XtKuGvyXHpkNGXMSUoGIpEQblJ4DJgEtDOzB4H/onUUpI499q8l5GzN58FRR9G4oUYpi0RCuL2P/m5ms9jbEDzS3RdELiyRfS36dgd/+Wg5FwzpxHHdW0c7HJGEVZ0J8ZoAZVVI6vcndaa01Ll90hyapzTktrP7RDsckYQWVvWRmf2a0CR1rYA2wDPB+s0iEffKzDXMWrWV28/uQyutnywSUeHeKfwYGOTuBQBmNo7Q4LMHIhWYCMCmnYWMe28hx3ZrxQ+HdIp2OCIJL9yG5pVAxSknGwPLaj0akf08+O4Cdu8p5sFRR2k9BJE6EO56CoXAPDP7INg+nVAPJJGI+XjpJibNXsu1px7BEW2bRTsckXoh3PUUZhHqklpmekSiEQkUFJVw5+S5dG3dhGtOOSLa4YjUG1WupxAMWpvg7mGveSByuJ6cvowVm3bx/BVDtXKaSB2qsk3B3UuADE1rIXVl2cadPDl9GecO6MiJPbVmhkhdCrf30UrgYzN7i9C6BwC4+6ORCErqL3fnrslzaZzcgDtHaEyCSF0LNynkBj8N2HcFNpFaNWn2Wj5ZtpkHRvanbXOtsSxS18Kd5uJeADNrHtr0nRGNSuqlbbv38OC7CxjUpQU/Gar1t0WiIdwRzf2DabPnEuqaOsvM+kU2NKlvxr23kG35Rfxm1FE0aKAxCSLREO7gtfHADe7e1d27AjcCf4lcWFLfzFi5hZdnrOGK73ajT4e0aIcjUm+FmxSauvuHZRvuPh1oGpGIpN7ZU1zK7RPnkNkilV+d1jPa4YjUa+E2NC83s7uA54PtS4AVkQlJ6pu//Gc5Szbs5G+js2nSqDoT94pIbQv3TuGnQAYwkdDI5jbA5ZEKSuqP1Zt389i0JZzZrz3D+rSLdjgi9V64vY+2AtcBBCOcm7p7XiQDk8Tn7tz15lwaNjDuPrdvtMMREcLvffSimaWZWVNgHrDIzG6ObGiS6N6ds45/L97IjWf0pkO61m0SiQXhVh/1De4MRgL/ALoAl0YqKEl8eQVF3Pv2fPpnpjH6hKxohyMigXCTQrKZJRNKCm+6exGhKbRrxMxamNnrZrbQzBaY2fFm1srMPjCzJcHvljU9vsS+R6YuYvPOQn4z6iiSNCZBJGaEmxT+j9D8R02Bj8ysK3A4bQp/BKa4+5HAAGABMBaY5u49gWnBtiSgr9Zs4/nPVvE/x2dxdKcW0Q5HRCow95p94Tezhu5eXIPXpQFfA929wsnNbBFwsruvM7MOwHR3732oY2VnZ/vMmTMPtYvEmOKSUs7908ds3lXIP284ieYpydEOSaTeMbNZ7p5d2XNVrbx2QxXHrsksqd2BjcAzZjaA0AI+vwTaufs6gCAxtD1ITGOAMQBdumh+nHjz7Ccrmb8ujycuHqyEIBKDqqo+ah78ZAM/BzKDn6uBmvYhbAgMBp5090GEpuIOu6rI3ce7e7a7Z2dkaK79eJK7LZ9HP1jMKb0zOKt/+2iHIyKVqGrltbLZUd8HBrv7jmD7HuC1Gp4zB8hx98+D7dcJJYX1ZtahQvXRhhoeX2LUPW/No9Sd+87rj5kal0ViUbgNzV2APRW29wBZNTmhu38LrDGzsvaCYcB84C1gdFA2GnizJseX2PT+vG95f/56fjmsF51bNYl2OCJyEOFONPM88IWZTSLUFXUU8NxhnPda4O/BEp/LCU2Z0QB41cyuAFYDFxzG8SWG7Cos5p635tG7XXOuPLFbtMMRkUMId5qLB81sCvDdoOhyd59d05O6+1eE2in2N6ymx5TY9fsPFpO7vYA3fjKI5KRwb05FJBrCnpLS3WeZ2RogBcDMurj76ohFJglhXu52nvlkJT8e2oUhXVtFOxwRqUK4cx+da2ZLCE2X/e/g93uRDEziX0mpc/ukubRskszYM4+MdjgiEoZw7+XvB44DFrt7N+A04OOIRSUJ4cXPV/H1mm3c+f2+pDfRmASReBBuUihy981AAzNrEKzCNjByYUm825BXwENTFvHdI9pw3sCO0Q5HRMIUbpvCNjNrBnxEqNfQBqDaU1xI/XHfO/MpLCnl/pEakyAST8K9UzgP2A1cD0wBlgHnRCooiW//XryRd75ZxzUnH0G3NlrKWySehNsldVfwsBSYELlwJN4VFJVw1+S5dM9oytUnd492OCJSTVolXWrV4/9awuotu3nxqmNp3DAp2uGISDVpJJHUmiXrdzD+o+WcPziTE3q0iXY4IlID1U4KZtbSzI6ORDASv0pLnTsmzaVp44bccXafaIcjIjUU7uC16WaWZmatCC2Q84yZ1WQtBUlQr8/K4YuVW7jtrCNp3axxtMMRkRoK904h3d3zgPOBZ9x9CKEBbCKhtZbfW8AxWS25YEjnaIcjIoch3KTQMFjj4EfAOxGMR+LQg/9YwM6CYh4cdRQNGmhMgkg8Czcp3AdMBZa5+wwz6w4siVxYEi8+WbaJiV+uZcz3utOrXfNohyMihynccQqvUWGlNXdfDvwgUkFJfCgsLuHOSXPp3CqVa0/tGe1wRKQWhNvQ3MvMppnZ3GD7aDO7M7KhSax7avpylm/axf3n9Se1kcYkiCSCcKuP/gLcBhQBuPs3wEWRCkpi34pNu/jz9KWMOLoDJ/duG+1wRKSWhJsUmrj7F/uVaUK8esrduXPyHBonNeDXI/pGOxwRqUXhJoVNZtaD0PrMmNkPgXURi0pi2ptf5fLx0s3ccmZv2qalRDscEalF4c59dA0wHjjSzNYSWnntkohFJTFr++4iHnh3PgM6t+Anx3aNdjgiUsvC7X20HDjNzJoCDdx9R2TDklg1bspCtu4uYsJP+5OkMQkiCeeQScHMbjhIOQDurqku6pFZq7bw0herufK73ejXMT3a4YhIBFR1p6DRSMLk2Wt5aOpCcrcVkGTQs22zaIckIhFyyKTg7vdG6sRmlgTMBNa6+4hgsr1XgCxgJfAjd98aqfNLeCbPXstNr31NcakDUOJwx+S5NE5OYuSgzChHJyK1LdzBa8+Y2dP7/xzmuX8JLKiwPRaY5u49gWnBtkTZw1MXlSeEMsWlzsNTF0UpIhGJpHC7pL4DvBv8TAPSgJ01PamZdQK+D/y1QvF57F3qcwIwsqbHl9qTuy2/WuUiEt/C7X30RsVtM3sJ+OdhnPcPwC3s22bRzt3XBedbZ2aVDpM1szHAGIAuXbocRggSjhZNktm6u+iA8o4tUqMQjYhEWk2X4+wJ1OgT2cxGABvcfVZNXu/u4909292zMzIyanIICdOmnYUUFJUcUN6wgXHz8N5RiEhEIi2sOwUz20EwmjnwLXBrDc/5HeBcMzsbSAHSzOwFYL2ZdQjuEjoAG2p4fKkl9709n5JSGHtmb57/bDW52/Lp2CKVm4f3ViOzSIIKt/qo1rqmuvtthCbXw8xOBm5y90vM7GFgNDAu+P1mbZ1Tqu9fC9fz1te5XH9aL64++QiuPvmIaIckInUg3N5Ho8wsvcJ2CzMbWcuxjANON7MlwOnBtkTBzsJi7pg0l17tmvHzk3tEOxwRqUPhtinc7e7byzbcfRtw9+Ge3N2nu/uI4PFmdx/m7j2D31sO9/hSMw9PWci3eQX89vyjadSwps1OIhKPwv0fX9l+4U6mJ3Fk1qotPPfZKkYfn8WQri2jHY6I1LFwk8JMM3vUzHqYWXcz+z1Qo95DErsKi0u49Y05dExP5Sb1LhKpl8JNCtcCewhNQ/EqkE9oOm1JIE9OX8bSDTt5YFR/mjXWjaBIfRRu76NdwFgza+buNR7JLLFr8fod/PnDpZw3sCOnaHlNkXor3N5HJ5jZfGB+sD3AzJ6IaGRSZ0pKnVvf+IZmjRtqeU2Rei7c6qPfA8OBzQDu/jXwvUgFJXXrhc9WMXv1Nu4a0ZfWzRpHOxwRiaKw+xu6+5r9ig6c/0Diztpt+Tw0ZSHf65XBKI1SFqn3wm1NXGNmJwBuZo2A69h32muJQ+7OnZPmUOrw4Mj+5SvqiUj9Fe6dwtWEehtlAjnAQNT7KO699XUuHy7ayE3De9O5VZNohyMiMSDc3kebgIsjHIvUoS279nDv2/MZ0Cmdy07IinY4IhIjDpkUzOxx9p0ddR/ufl2tRyR14oF355OXX8S4K48lqYGqjUQkpKo7hZl1EoXUqY8Wb2Til2v5xSlH0KdDWrTDEZEYcsik4O4TKm6bWfNQsQawxavde4q5fdIcumc05RenajpsEdlXuIPX+pvZbGAuMN/MZplZv8iGJpHw6PuLydmaz7jzjyYlOSna4YhIjAm399F44AZ37+ruXYAbgb9ELiyJhK/XbOPpj1dw8bFdGNqtVbTDEZEYFG5SaOruH5ZtuPt0oGlEIpKIKCop5dY3viGjeWNuPevIaIcjIjEq3MFry83sLuD5YPsSYEVkQpJIGP/RchZ+u4Pxlw4hLSU52uGISIwK907hp0AGMBGYFDy+PFJBSe1atnEnf5y2hO8f1YEz+rWPdjgiEsPCHby2ldDUFhJnSkud2ybOIaVhA+4+VzOgisihVTV47a1DPe/u59ZuOFLbXp6xhi9WbOGhHxxN2+Yp0Q5HRGJcVXcKxwNrgJeAzwENfY0j324v4Lf/WMAJPVpzQXanaIcjInGgqqTQHjgd+DHwE+Bd4CV3nxfpwOTwuDt3vTmXPSWl/GbUUZoBVUTCcsiGZncvcfcp7j4aOA5YCkw3s2vrJDqpsSlzv+WD+eu5/vReZLVR72ERCU+VvY/MrLGZnQ+8QGi67McI9UKqETPrbGYfmtkCM5tnZr8MyluZ2QdmtiT43bKm56jvtu8u4tdvzaNfxzSu/G63aIcjInHkkEnBzCYAnwCDgXvd/Rh3v9/d1x7GOYuBG929D6G7j2vMrC8wFpjm7j2BacG21MBv31vAll17+N0PjqZhUtiL64mIVNmmcCmwC+gFXFehXtoITYxX7Sk23X0dsC54vMPMFhBavOc84ORgtwnAdODW6h6/vvtk2SZenrGGn53Unf6Z6dEOR0TiTFWzpEb0a6aZZQGDCPVsahckDNx9nZm1PchrxgBjALp06RLJ8OJOQVEJt0+cQ9fWTfjVsF7RDkdE4lDU6hbMrBnwBvArd88L93XuPt7ds909OyMjI3IBxqE//HMJKzfv5rejjiK1kWZAFZHqi0pSMLNkQgnh7+5e1mi93sw6BM93ADZEI7Z4NXftdv7yn+VcmN2ZE45oE+1wRCRO1XlSsFDDxN+ABe7+aIWn3gJGB49HA2/WdWzxqriklLETv6Flk0bcfnafaIcjInEs3FlSa9N3CDVgzzGzr4Ky24FxwKtmdgWwGrggCrHFpac/XsHctXk8cfFg0ptoBlQRqbk6Twru/l8OPl3GsLqMJRGs2ryLRz9YzOl923FWf82AKiKHR53Y45h7aAbU5AYNuP+8/prKQkQOm5JCHHttVg6fLNvMrWcdSft0zYAqIodPSSFObdhRwIPvLmBoVit+MlTjNUSkdigpxKl7355P/p4SfvuDo2jQQNVGIlI7lBTi0Afz1/PuN+u4btgR9MhoFu1wRCSBKCnEmbyCIu6aPJcj2zdnzPd6RDscEUkw0RinIIfhoSkLWb+jgKcuHUKjhsrpIlK79KkSR2as3MILn63m8hO6MbBzi2iHIyIJSEkhThQUlTD2jW/o1DKVm4ZrBlQRiQxVH8WJJz5cyrKNu5jw06E0aaR/NhGJDN0pxIGF3+bxxPRlnD8ok5N6abpwEYkcJYUYV1LqjH1jDmmpydw5om+0wxGRBKekEOMmfLKSr9Zs4+5z+tKqaaNohyMiCU5JIYat2bKbR95fxCm9Mzh3QMdohyMi9YCSQoxyd+6YPBeAB0YdpRlQRaROKCnEqDe/yuWjxRu5ZXhvMlukRjscEaknlBRi0Oadhdz79jwGdWnBpcdnRTscEalH1OE9hkyevZaHpy5i7bZ8AMb0bUeSZkAVkTqkO4UYMXn2Wm6bOKc8IQA8Nm0pk2evjWJUIlLfKClEWf6eEublbufet+eRX1Sy73NFJTw8dVGUIhOR+kjVR3XA3fk2r4DlG3exbOPOfX5XvDOoTG4Vz4uI1CYlhVpUUFTC8o27WL5pJ8s2BL837mTFxl3s2rP3LqBpoyS6ZzTjmKyWXJjRmR4Zzbj37Xls2FF4wDE7queRiNQhJYVqcnc27Chk2cadLNu4i+XB72UbdpK7PR/3vftmtkile0ZTsrNb0SOjKT0ymtE9oxnt0hofMO6gqKSU2ybO2acKKTU5iZuH966rtyYiEntJwczOBP4IJAF/dfdxtX2Osl4+udvy6dgilZuH92bkoMx99ikoKmHl5l2hqp4NO1m+aW+Vz87C4vL9mjRKontGU4Z0bcmPMjrTPfjw79amKamNksKOqez8VcUlIhJJ5hW/2kaZmSUBi4HTgRxgBvBjd59f2f7Z2dk+c+bMap2jrJdPxW/kjRo24LyBHWneOLm8yidn677f+jump9CjbTO6t2ka/G5Gj7ZNaZ+WotHGIhJXzGyWu2dX9lys3SkMBZa6+3IAM3sZOA+oNCnUxMNTFx3Qy2dPcSmvzcwhNTmJbm2aMrBzS84f1Kk8CXTPaKo1DESkXoi1T7pMYE2F7Rzg2No8wcF68xgw797hNNBgMRGpx2JtnEJln8j71G+Z2Rgzm2lmMzdu3FjtExysN0/HFqlKCCJS78VaUsgBOlfY7gTkVtzB3ce7e7a7Z2dkVH8VspuH9yY1ed8GYPXyEREJibXqoxlATzPrBqwFLgJ+UpsnUC8fEZGDi6mk4O7FZvYLYCqhLqlPu/u82j7PyEGZSgIiIpWIqaQA4O7/AP4R7ThEROqjWGtTEBGRKFJSEBGRckoKIiJSTklBRETKxdTcR9VlZhuBVdGOIwLaAJuiHUQc0fWqHl2v6knE69XV3Ssd6BXXSSFRmdnMg01WJQfS9aoeXa/qqW/XS9VHIiJSTklBRETKKSnEpvHRDiDO6HpVj65X9dSr66U2BRERKac7BRERKaekICIi5ZQURESknJJCnDGzpmY2y8xGRDuWWGdmI83sL2b2ppmdEe14YlHw9zQhuE4XRzueeJDof1dKCnXEzJ42sw1mNne/8jPNbJGZLTWzsWEc6lbg1chEGTtq43q5+2R3vwq4DLgwguHGlGpeu/OB14PrdG6dBxsjqnPNEv3vSkmh7jwLnFmxwMySgD8DZwF9gR+bWV8zO8rM3tnvp62ZnQbMB9bXdfBR8CyHeb0qvPTO4HX1xbOEee0ILXm7JtitpA5jjDXPEv41K5OQf1cxt8hOonL3j8wsa7/iocBSd18OYGYvA+e5+2+BA6qHzOwUoCmhP9B8M/uHu5dGNvLoqKXrZcA44D13/zLCIceM6lw7QuuidwK+oh5/SazONTOzBSTw35WSQnRlsvdbGoT+gx57sJ3d/Q4AM7sM2JSoCeEQqnW9gGuB04B0MzvC3Z+KZHAx7mDX7jHgT2b2feDtaAQWww52zRL670pJIbqskrIqRxO6+7O1H0pcqNb1cvfHCH3oyUGunbvvAi6v62DixMGuWUL/XdXb28UYkQN0rrDdCciNUizxQNer5nTtqq9eXjMlheiaAfQ0s25m1gi4CHgryjHFMl2vmtO1q756ec2UFOqImb0EfAr0NrMcM7vC3YuBXwBTgQXAq+4+L5pxxgpdr5rTtas+XbO9NCGeiIiU052CiIiUU1IQEZFySgoiIlJOSUFERMopKYiISDklBRERKaekIHHNzErM7KsKP1l1cM5zy6ZRDubW71vVayo5xh/M7HvB4+lmlh08zjKzJWY2/BCvHWhmZ9fgnC+bWc/qvk7qFyUFiXf57j6wws/KSJ/Q3d9y93HB5khCs9aGzcxaAce5+0f7lXciNFDqRnefeohDDASqnRSAJ4FbavA6qUeUFCThmNlKM2sTPM42s+nB4wwz+8DMvjSz/zOzVRX2m2yhFe3mmdmYCsc6M9j/azObFpRdZmZ/MrMTCC1M83Bwl9LDzL6s8NqeZjarkhB/CEzZr6w98D5wp7u/Fbx+qJl9Ymazg9+9g+kW7gMuDM55oZndY6HV094P3vv5ZvaQmc0xsylmlhyc4z/AaWamiTDloJQUJN6lVqg6mlTFvncD/3L3wcAkoEuF537q7kOAbOA6M2ttZhnAX4AfuPsA4IKKB3P3TwjNhXNzcJeyDNhuZgODXS4ntHjL/r4D7J8sngP+5O6vVShbCHzP3QcBvwZ+4+57gsevBOd8Jdi3B/B9QmskvAB86O5HAflBOcFU60uBAVVcJ6nH9I1B4l2+uw8Mc9/vAqMA3H2KmW2t8Nx1ZjYqeNwZ6AlkAB+5+4rgNVvCOMdfgcvN7AZCSzUOrWSfDsDG/cr+CVxqZs+6++6gLB2YELQDOJDMwb3n7kVmNgdIYu+dyBwgq8J+G4COHJiURADdKUhiKmbv33ZKhfLK5sfHzE4mtGjK8cEdwezgdUYY61vs5w1CyzeOAGa5++ZK9snfLy6Ah4DPgdcqVO/cT+gbf3/gnEpeU1EhlN8NFPneSc1K2ffLX0pwfpFKKSlIIloJDAke/6BC+X+BHwGY2RlAy6A8Hdjq7rvN7EjguKD8U+AkM+sWvKZVJefaATQv23D3AkKNxU8CzxwkvgXAEZWUXw/kAX8LlhJNB9YGz112sHNWUy8g4Wf6lJpTUpBEdC/wRzP7D/suRn8vcEbQGHwWsI7QB+wUoKGZfUPo2/lnAO6+ERgDTDSzr4FXONDLwM1BY3CPoOzvhO4w3j9IfO8CJ+9fGHy7H02oeumh4Oe3ZvYxoSqhMh8Cfcsamg91ISoys3aEqtvWhfsaqX80dbbUG2bWGChx92IzOx54shrtEdU5z01AurvfdYh9/guMcPdttX3+Q5zzeiDP3f9WV+eU+KOGZqlPugCvmlkDYA9wVW2fIOgB1QM4tYpdbwzi2VbbMRzCNuD5OjyfxCHdKYiISDm1KYiISDklBRERKaekICIi5ZQURESknJKCiIiUU1IQEZFy/x/+d1NVQjM4zwAAAABJRU5ErkJggg==\n",
      "text/plain": [
       "<Figure size 432x288 with 1 Axes>"
      ]
     },
     "metadata": {
      "needs_background": "light"
     },
     "output_type": "display_data"
    }
   ],
   "source": [
    "import pandas as pd\n",
    "import matplotlib.pyplot as plt\n",
    "\n",
    "thing = pd.read_table('./Tutorial_7_files/isotherm_simulation/nmol_1_sweep.dat', header=None, sep=' ')\n",
    "pd.DataFrame(thing).plot(x=0,y=1, yerr=2, capsize=2, marker='o', label='$CO_2$')\n",
    "plt.xscale('log')\n",
    "plt.xlabel('Fugacity (Katm)')\n",
    "plt.ylabel('Molecules adsorbed (n/uc)')"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "eb13b3bb",
   "metadata": {},
   "source": [
    "## More advanced use\n",
    "As I'm sure you can imagine, there are a lot of parameters you'll be able to sweep through using `simtask`. \n",
    "From the documentation, you can an alter any parameter in the `MainBlock` or the `molchempot` of the first species in an GCMC insert move.\n",
    "While you can only modify one parameter at a time, I'm sure it'd be possible to do nested loops across different `Measurement` templates e.g. for sequentially running isotherms at different temperatures. \n",
    "Why not give them a try as an extra challenge? \n",
    "Can you calculate the isosteric heat for $CO_2$ in this zeolite?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "46412409",
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "dlmontepython",
   "language": "python",
   "name": "dlmontepython"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.2"
  },
  "toc": {
   "base_numbering": 1,
   "nav_menu": {},
   "number_sections": true,
   "sideBar": true,
   "skip_h1_title": true,
   "title_cell": "Table of Contents",
   "title_sidebar": "Contents",
   "toc_cell": false,
   "toc_position": {},
   "toc_section_display": true,
   "toc_window_display": false
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
