{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "b10ea2af",
   "metadata": {},
   "source": [
    "#   Tutorial 2: Reproducing interaction potentials U(r) with EE & WL/RE methods"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "f4235fa3",
   "metadata": {},
   "source": [
    "Original author: Andrey V. Brukhno (andrey.brukhno{at}stfc.ac.uk)\n",
    "\n",
    "Modified for dlmontepython by Joe Manning (joseph.manning{at}manchester.ac.uk)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "965cc537",
   "metadata": {},
   "source": [
    "Reproducing *U(r)* with EE & WL/RE methods\n",
    "------------------------------------------\n",
    "\n",
    "In the limit of *infinite dilution*, i.e. when the solute concentration (or number density) is negligible, the potential of mean force approaches the underlying effective interaction for a pair of solute particles.\n",
    "\n",
    "This implies that if only two particles are present in the simulation box,\n",
    "\n",
    "$$\n",
    "   {\\rm PMF}=-\\ln g(r)\\ \\to\\ \\beta\\sum_k U_k(r)\n",
    "$$\n",
    "In this exercise we will see that it is actually the case.\n",
    "\n",
    "### Exercise 2.1: Using the EE method\n",
    "\n",
    "First, we will consider a pair of LJ particles in vacuum. \n",
    "\n",
    "Change to directory **tutorial6-1/fed_2ljp_ee/**, where you will find the input files for this exercise.\n",
    "\n",
    "Check the **cutoff**, energy **units** and **particle diameter** in FIELD file:\n",
    "\n",
    "``` \n",
    "   Lennard-Jones - 2 particles\n",
    "   CUTOFF 12.0\n",
    "   UNITS K\n",
    "   NCONFIGS 1\n",
    "   atoms 1\n",
    "   Lj core  1.0  0.00\n",
    "   MOLTYPES 1\n",
    "   test\n",
    "   MAXATOM  2\n",
    "   vdw 1 \n",
    "   Lj core  Lj core  lj 1.000000 3.000\n",
    "   FINISH\n",
    "   CLOSE\n",
    "```\n",
    "Check the **FED input** in CONTROL file:\n",
    "\n",
    "```\n",
    "   # FED block is started by 'use fed <flavor> <stride>' (must be in 'use' section on top)\n",
    "   use fed generic 1\n",
    "\n",
    "   # FED <method> <damping> <scaling> <update> [<smooth_itr> <smooth_1st> <smooth_last> <weight>]\n",
    "   fed method EE   0.5       1.125     500000    #3  10  250  #0.333\n",
    "\n",
    "   # FED order [param] <type> <nbins> <first> <last> <power>\n",
    "   fed order parameter com2    250     2.5     27.5   1\n",
    "   com1 mol 1 atom 1   # use COM1 for a group of molecules or atoms within molecules\n",
    "   com2 mol 1 atom 2   # use COM2 for a group of molecules or atoms within molecules\n",
    "   com  sampling correction 1 # entropy on a spherical surface (on the fly)\n",
    "   #com  sampling correction 2 # entropy in a spherical bin volume (a posteriori)\n",
    "   fed order parameter done\n",
    "\n",
    "   # close FED block started by 'use fed' directive\n",
    "   fed done\n",
    "```\n",
    "\n",
    "**NOTE:** Since at every MC step one of the FED-involved particles is moved, \n",
    "the stride for FED attempts can only be '1' (it will default to that otherwise)!"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "37a706c7",
   "metadata": {},
   "source": [
    "We start an EE iteration with 500,000 MC steps per EE update cycle initially. After two first cycles the update stride will be doubled every time the FED bias is updated (and stored in FEDDAT.\\<node\\>\\_\\<cycle\\> file), i.e. we are doing a sequence of 2 x 500,000; then 1,000,000; then 2,000,000; and so on...\n",
    "\n",
    "The EE method requires a **damping factor**, $\\eta$, for the energy feedback term, $\\Delta U_{bias}(R)$ (see above), which in this case is $\\eta_0=0.5$. The damping factor is scaled up after every EE iteration by the **scaling coefficient** as $\\eta_{k+1}=1.125\\ \\eta_k$ which is done with the same frequency as defined by the main update stride. \n",
    "\n",
    "The subsection defining the order parameter is started by directive '**fed order** ...' and closed by '**fed order [param] done**'. The first (opening) record in this subsection specifies: the parameter grid number, the allowed range of variation (working window), and the power of incremental bin size (if greater than 1 in absolute value).\n",
    "\n",
    "The specs for the **two COM groups** are self-evident: each contains a single atom (indeces 1 and 2) \n",
    "in the same 'molecule 1' (under name 'test' in FIELD and CONFIG). The third directive in the 'fed order' \n",
    "subsection specifies how the bias unfolding correction is to be accounted for. In this case we opted \n",
    "for the 'on the fly' correction that accounts for the *ideal entropy on a sphere* of the actual (current) \n",
    "radius, $\\sim 2\\ln R$.\n",
    "\n",
    "Further in CONTROL file the number of MC steps and strides for collecting stats should be commensurate \n",
    "with those for FED:\n",
    "\n",
    "```\n",
    "   temperature          1.0         # Reduced temperature\n",
    "   nocoul all                       # No electrostatics\n",
    "   #distewald                       # Distributed Ewald sums in parallel runs\n",
    "\n",
    "   equilibration        0           # Equilibration period (not for FED calcs)\n",
    "   steps                4000000     # Total number of MC steps 2^(<iters>-1)*<update>\n",
    "   print                1000000     # Frequency of printing to OUTPUT\n",
    "   stats                 100000     # frequency of printing stats to PFILE\n",
    "   check                1000000     # Check rolling energies vs recalculated from scratch\n",
    "   sample rdfs     300 30.0 100     # Sampling grid, bin size & frequency for RDF calculation\n",
    "   sample coords          10000     # Frequency of storing configuration in trajectory\n",
    "\n",
    "   acceptatmmoveupdate  1000000000  # DO NOT update max atom displacement\n",
    "   maxatmdist           10.000      # Max atom displacement\n",
    "   move atoms         1 100         # Move atoms\n",
    "   Lj core                          # Name & type of atom to move\n",
    "\n",
    "   start simulation\n",
    "```\n",
    "Run the simulation (serial version) and check the output::\n",
    "\n",
    "```\n",
    "   > ls FED*\n",
    "   FEDDAT.000_001  FEDDAT.000_002  FEDDAT.000_003  FEDDAT.000_004\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "afd5977a",
   "metadata": {},
   "source": [
    "Launch gnuplot and plot the FED data, which is the second column in FEDDAT files (to quit gnuplot type 'q')::\n",
    "\n",
    "```\n",
    "   [tutorial6-1/fed_2ljp_ee]$ gnuplot\n",
    "   gnuplot> plot 'FEDDAT.000_001' u 1:2 w l t \"EE iter 1\", 'FEDDAT.000_002' u 1:2 t \"EE iter 2\" w l, \\\n",
    "   'FEDDAT.000_003' u 1:2 w l t \"EE iter 1\", 'FEDDAT.000_004' u 1:2 w l t \"EE iter 4\"\n",
    "```\n",
    "\n",
    "Now let's add the known target (LJ potential) and zoom in by providing X and Y ranges::\n",
    "```\n",
    "   gnuplot> lj(arg) = 4*( (3.0/arg)**12 - (3.0/arg)**6 )\n",
    "   gnuplot> plot [x=2:10] [y=-1.5:1.5] 'FEDDAT.000_001' u 1:2 w l t \"EE iter 1\", \\\n",
    "   'FEDDAT.000_002' u 1:2 t \"EE iter 2\" w l, 'FEDDAT.000_003' u 1:2 w l t \"EE iter 3\", \\\n",
    "   'FEDDAT.000_004' u 1:2 w l t \"EE iter 4\", 'FEDDAT.000_001' u 1:(lj($1)) w l lc 'black' lt 0 t \"Target U(r)\"\n",
    "```\n",
    "\n",
    "<img src=\"images/FED-2ljp-ee-iters4.png\" alt=\"Demonstration of the Metropolis algorithm\" class=\"bg-primary\" width=\"500px\"> \n",
    "\n",
    "Finally, also check the EE population histograms, i.e. probability distributions, which is the third column in FEDDAT files::\n",
    "```\n",
    "   gnuplot> plot [] [y=0:12000] 'FEDDAT.000_001' u 1:3 w l t \"EE iter 1\", \\\n",
    "   'FEDDAT.000_002' u 1:3 t \"EE iter 2\" w l, 'FEDDAT.000_003' u 1:3 w l t \"EE iter 3\", \\\n",
    "   'FEDDAT.000_004' u 1:3 w l t \"EE iter 4\"\n",
    "```\n",
    "<img src=\"images/FED-2ljp-ee-iters4-probs.png\" alt=\"Demonstration of the Metropolis algorithm\" class=\"bg-primary\" width=\"500px\"> \n",
    "\n",
    "\n",
    "#### What might go wrong?\n",
    "\n",
    "Questions to answer:\n",
    "\n",
    "* Was the entire order parameter range covered appropriately?\n",
    "* **Are the histograms sufficiently flat?**\n",
    "* Can one improve of the stats by varying $\\eta_0$, the scaling-up coef, or the bias update stride?\n",
    "* Would smoothing upong updating the bias help?"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "4ae9776d",
   "metadata": {},
   "source": [
    "#### Refinement production run is necessary!\n",
    "\n",
    "Let's store away the current results::\n",
    "<!-- \n",
    "   [tutorial6-1]/fed_2ljp_ee$ mkdir equil-fed1/\n",
    "   [tutorial6-1]/fed_2ljp_ee$ cp CON* FIELD equil-fed1/\n",
    "   [tutorial6-1]/fed_2ljp_ee$ mv *.0??* equil-fed1/ -->\n",
    "\n",
    "Now amend CONTROL file:\n",
    "```\n",
    "   # FED <method> <damping> <scaling> <update> [<smooth_itr> <smooth_1st> <smooth_last> <weight>]\n",
    "   fed method EE   0.7       1.125    2000000    #3  10  250  #0.333\n",
    "\n",
    "   # FED order [param] <type> <nbins> <first> <last> <power>\n",
    "   fed order parameter com2    250     2.5     27.5   -1\n",
    "   ...\n",
    "   steps                8000000     # Total number of MC steps 2^(<iters>-1)*<update>\n",
    "```\n",
    "\n",
    "and re-run.\n",
    "\n",
    "Ooops... \n",
    "\n",
    "Due to '-1' at the end of '**fed order**' directive, we are asking for a seed file **FEDDAT.000** \n",
    "which we forgot to provide! Of course, we meant to use the FED bias data from the latest iteration::\n",
    "\n",
    "```\n",
    "   [tutorial6-1]/fed_2ljp_ee$ cp equil-fed1/FEDDAT.000_004 ./FEDDAT.000\n",
    "```\n",
    "\n",
    "restart it once again and redo the analysis of the production run.\n",
    "\n",
    "<img src=\"images/FED-2ljp-ee-iters7-probs.png\" alt=\"Demonstration of the Metropolis algorithm\" class=\"bg-primary\" width=\"500px\"> \n",
    "\n",
    "\n",
    "<img src=\"images/FED-2ljp-ee-iters7.png\" alt=\"Demonstration of the Metropolis algorithm\" class=\"bg-primary\" width=\"500px\"> \n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "fd8c7658",
   "metadata": {},
   "source": [
    "### Exercise 1.2: Using the WL method with replica-exchange in *T*\n",
    "\n",
    "Next, we will apply the Wang-Landau method to the same system of a pair of LJ particles. \n",
    "\n",
    "Change to directory **tutorial6-1/fed_2ljp_wl/**, where the input files are found.\n",
    "\n",
    "We will combine the WL FED calculation with the replica-exchange (RE) parallel tempering technique \n",
    "so that FED(R) will be evaluated for four temperatures at once!\n",
    "\n",
    "To this end, the following two lines in CONTROL file are added (in the '**use**' section below the title):\n",
    "\n",
    "```\n",
    "   use ortho                  # use orthorhombic PBC for speed-up (if the cell is cubic)\n",
    "   use repexch 4  -0.2  1000  # Replica-Exchange with 4 replicas, delta T = -0.2, sampling every 1000 MC steps\n",
    "```\n",
    "The FED input to use the WL method is as follows:\n",
    "\n",
    "```\n",
    "\n",
    "   # FED block is started by 'use fed <flavor> <stride>' directive\n",
    "   use fed generic 1\n",
    "   \n",
    "   # FED <method> <droplet> <scaling> <update> [<smooth_itr> <smooth_1st> <smooth_last> <weight>]\n",
    "   fed method WL   0.002     0.5       500000    #3  10  250  #0.333\n",
    "\n",
    "   # FED order [param] <type> <nbins> <first> <last> <power>\n",
    "   fed order parameter com2    250     2.5     27.5   1\n",
    "```\n",
    "Make sure that reading the FED bias from input is turned off, i.e. '<**power**>' is set ot 1 in '**fed order** ...' directive. \n",
    "Also, make sure that the rest of the input in CONTROL corresponds to our initial run comprising 4,000,000 MC steps in total.\n",
    "\n",
    "Similar to EE, the WL iteration initially starts with 500,000 MC steps per WL update cycle. The update stride \n",
    "is doubled in the same manner as for EE: 2 x 500,000; then 1,000,000; then 2,000,000; and so on...\n",
    "\n",
    "The WL method requires an initial **droplet** value, $\\delta$ (recall the WL illustration), which in this case is $\\delta_0=0.002$. The **scaling coefficient** acts as follows: $\\delta_{k+1}=0.5\\ \\delta_k$ with the same frequency as defined by the main update stride. \n",
    "\n",
    "**NOTE:** Even though there are no charges (no electrostatic interactions) in this case, one has to invoke \n",
    "the '**distewald**' directive for *any parallel run*!\n",
    "\n",
    "```\n",
    "\n",
    "   temperature          1.0         # Reduced temperature\n",
    "   nocoul all                       # No electrostatics\n",
    "   distewald                        # Distributed Ewald sums in parallel runs\n",
    "\n",
    "   equilibration        0           # Equilibration period (not for FED calcs)\n",
    "   steps                8000000     # Total number of MC steps 2^(<iters>-1)*<update>\n",
    "   print                1000000     # Frequency of printing to OUTPUT\n",
    "   stats                 100000     # frequency of printing stats to PFILE\n",
    "   check                1000000     # Check rolling energies vs recalculated from scratch\n",
    "   sample rdfs     300 30.0 100     # Sampling grid, bin size & frequency for RDF calculation\n",
    "   sample coords          10000     # Frequency of storing configuration in trajectory\n",
    "\n",
    "   acceptatmmoveupdate  1000000000  # DO NOT update max atom displacement\n",
    "   maxatmdist           10.000      # Max atom displacement\n",
    "   move atoms         1 100         # Move atoms\n",
    "   Lj core                          # Name & type of atom to move\n",
    "\n",
    "   start simulation\n",
    "```\n",
    ".. If you copied and edited the correct CONTROL file from **tutorial6-1/fed_2ljp_ee/equil-fed1/**, everything should be ready by now.\n",
    "\n",
    "To run the replica exchange parallel job, you have to ensure that you are not running an interactive session, but instead submit the job to a parallel queue::\n",
    "```\n",
    "  [tutorial6-1/fed_2ljp_wl]$ sbatch parallel.sub\n",
    "```\n",
    "Run the simulation and check the output (order can be different)::\n",
    "```\n",
    "   [tutorial6-1/fed_2ljp_wl]$ ls FED*\n",
    "   FEDDAT.000_001  FEDDAT.000_002  FEDDAT.000_003  FEDDAT.000_004  FEDDAT.000_005\n",
    "   FEDDAT.001_001  FEDDAT.001_002  FEDDAT.001_003  FEDDAT.001_004  FEDDAT.001_005\n",
    "   FEDDAT.002_001  FEDDAT.002_002  FEDDAT.002_003  FEDDAT.002_004  FEDDAT.002_005\n",
    "   FEDDAT.003_001  FEDDAT.003_002  FEDDAT.003_003  FEDDAT.003_004  FEDDAT.003_005\n",
    "```\n",
    "Launch gnuplot and plot the FED data::\n",
    "```\n",
    "   [tutorial6-1/fed_2ljp_wl]$ gnuplot\n",
    "   gnuplot> plot [x=2:10] [y=-3:2] 'FEDDAT.000_005' u 1:2 w l t \"Replica 1, WL iter 5\", \\\n",
    "   'FEDDAT.001_005' u 1:2 t \"Replica 2, WL iter 5\" w l, 'FEDDAT.002_005' u 1:2 w l t \"Replica 3, WL iter 5\", \\\n",
    "   'FEDDAT.003_005' u 1:2 w l t \"Replica 4, WL iter 5\"\n",
    "```\n",
    "\n",
    "<img src=\"images/FED-2ljp-wl-iters5.png\" alt=\"Demonstration of the Metropolis algorithm\" class=\"bg-primary\" width=\"500px\"> \n",
    "\n",
    "Check the histograms too::\n",
    "```\n",
    "   [tutorial6-1/fed_2ljp_wl]$ gnuplot\n",
    "   gnuplot> plot [] [y=14000:18000] 'FEDDAT.000_005' u 1:3 w l t \"Replica 1, WL iter 5\", \\\n",
    "   'FEDDAT.001_005' u 1:3 t \"Replica 2, WL iter 5\" w l, 'FEDDAT.002_005' u 1:3 w l t \"Replica 3, WL iter 5\", \\\n",
    "   'FEDDAT.003_005' u 1:3 w l t \"Replica 4, WL iter 5\"\n",
    "```\n",
    "<img src=\"images/FED-2ljp-wl-iters5-probs.png\" alt=\"Demonstration of the Metropolis algorithm\" class=\"bg-primary\" width=\"500px\"> \n",
    "\n",
    "\n",
    "- **Store away the data, amend the input and run the refinement (production) simulation on your own!**"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "27f07d4f",
   "metadata": {},
   "source": [
    "### Extra exercises\n",
    "\n",
    "- **Try to run EE/RE combination**\n",
    "- **Try to add charges (+1,-1) on the LJ particles and redo the FED calculations - what do you expect to obtain?**\n",
    "- **Study a more physically meaningful case:**\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "94a0e1d0",
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "dlmontepython",
   "language": "python",
   "name": "dlmontepython"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.2"
  },
  "toc": {
   "base_numbering": 1,
   "nav_menu": {},
   "number_sections": true,
   "sideBar": true,
   "skip_h1_title": true,
   "title_cell": "Table of Contents",
   "title_sidebar": "Contents",
   "toc_cell": false,
   "toc_position": {},
   "toc_section_display": true,
   "toc_window_display": false
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
