"""Tests for DLMolecule ASE interface class

Author: Joe Manning
Date: 9-6-23
"""

import os.path
import unittest
import numpy

from dlmontepython.htk.sources import dlmolecule as dlm
from dlmontepython.htk.sources.dlfieldspecies import AtomType, Atom, Molecule

class DLMoleculeTest(unittest.TestCase):

    """Instantiation"""

    def from_json(self):

        input_json = {
            'name': 'He',
            'positions': [(0,0,0)],
            'tags':  [0],
            'charges': [0],
        }
        LJ = dlm.DLMolecule(
            name='LJ',
            molecule = input_json,
            tags = {0: 'He'}, #helium for ease of reading back in with ASE later on
            potentials = {
            0: [1,1,0]
            }
            )
        
        self.assertEquals(LJ._get_field_atomstype(0,'He'),
                          AtomType('He', 'core', 4.0026, round(0,5))
                          )
        self.assertEquals(LJ._config_atoms_stringify()[0],
                          'He core 0 0 0 0'
                         )
        self.assertEquals(len(LJ.molecule), 1)
        self.assertEquals(LJ._get_field_atoms()[0],
                          Atom('He', 'core', 0,0,0,4.0026,0,None))
        self.assertEquals(LJ.get_field_molecule(),
                          Molecule('LJ').add_atom(
            Atom('He', 'core', 0,0,0,4.0026,0,None)
            )
                                  )


    def from_ase(self):
        try:
            from ase import Atoms
        except ModuleNotFoundError as e:
            print("Unable to test DLMolecule, no module named ase found")
            print(e.message)
        else:
            LJ = dlm.DLMolecule(
                name = 'LJ',
                molecule = Atoms(
                'He',
                positions=[(0,0,0)],
                tags = [0],
                charges = [0]
                ),
                tags = {0: 'He'}, #helium for ease of reading back in with ASE later on
                potentials = {
                0: [1,1,0]
                }
            )
            
            self.assertEquals(LJ._get_field_atomstype(0,'He'),
                          AtomType('He', 'core', 4.0026, round(0,5))
                          )
            self.assertEquals(LJ._config_atoms_stringify()[0],
                            'He core 0 0 0 0'
                            )
            self.assertEquals(len(LJ.molecule), 1)
            self.assertEquals(LJ._get_field_atoms()[0],
                            Atom('He', 'core', 0,0,0,4.0026,0,None))
            self.assertEquals(LJ.get_field_molecule(),
                            Molecule('LJ').add_atom(
                Atom('He', 'core', 0,0,0,4.0026,0,None)
                )
                                    )
