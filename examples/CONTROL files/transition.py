#!/usr/bin/env python
# coding: utf-8

from dlmontepython.htk.sources import dlcontrol, dlmove, dlfedmethod, dlfedorder, dlfedflavour
from collections import OrderedDict
from random import randint

# A worked example of a control file (as a python object)
fed_block = dlcontrol.FEDBlock(
    flavour=dlfedflavour.Generic(),
    method= dlfedmethod.TransitionMatrix(
        nout=10000,
        n_upd=1000,
        mode="new",
        tri=True
    ),
    orderparam= dlfedorder.FEDOrderParameter(
        name="nmols",
        ngrid=161,
        xmin=-0.5,
        xmax=160.5,
        npow=1,
        win='win',
        winmin=-0.5,
        winmax=8.5
    )

)


TransitionMatrixExample = dlcontrol.CONTROL(
    title='Adsorption worked example',

    use_block=dlcontrol.UseBlock(  # overarching simulation parameters e.g. free energy schemes
        use_statements=OrderedDict(
            {
                'gaspressure': None,  # Use fugacity, not chemical potential
                'ortho': None  # Use various speedups of an orthogonal simulation cell
            }
        ),
        fed_block=fed_block
    ),

    main_block=dlcontrol.MainBlock(  # General simulation parameters
        statements=OrderedDict(
            {
                # Defining random seeds
                'seeds': OrderedDict(
                    {
                        'seed0': randint(0, 99),
                        'seed1': randint(0, 99),
                        'seed2': randint(0, 99),
                        'seed3': randint(0, 99)
                    }
                ),

                # statements on simulation length and output frequency
                'archiveformat': 'dlmonte',  # Trajectory output file format
                'equilibration': 0,  # Simulation burn in steps
                'steps': int(1e6),  # Total simulation steps
                'check': int(1e3),  # Frequency of checking internal energy calculations
                'stack': int(1e3),  # Stack size for block averaging
                'yamldata': int(1e3),  # Frequency of yamldata file output
                'print': int(1e4),  # Frequency of configuration printing output

                # Statements on simulation physical parameters
                'temperature': 77.,  # In Kelvin
                'ewald precision': 1e-6,

                # Statements on Monte Carlo move updates
                'acceptatmmoveupdate': 200,
                'acceptmolrotupdate': 200
            }
        ),
        # MC move definition
        moves=[
            dlmove.InsertMoleculeMove(
                pfreq=50,  # proportional frequency of move type
                rmin=0.7,  # auto-reject distance for insert attemts, in angstrom
                movers=[
                    {
                        'id': 'argon',  # Molecule name, cross-references against FIELD and CONFIG files
                        'molpot': 5e-7  # chemical potential as defined in the use block
                    }
                ]
            ),
            dlmove.MoleculeMove(
                pfreq=25,
                movers=[
                    {
                        'id': 'argon'
                    }
                ]
            ),
            dlmove.RotateMoleculeMove(
                pfreq=25,
                movers=[
                    {
                        'id': 'argon'
                    }
                ]
            )
        ],

        # Writing information to other files
        samples=OrderedDict(
            {
                'coords': OrderedDict(  # Atom coordinates
                    {
                        'nfreq': int(1e6)
                    }
                )
            }
        )
    )

)

if __name__ == '__main__':
    TransitionMatrixExample.use_block.use_statements.pop('ortho')
    TransitionMatrixExample.main_block.statements['noewald'] = 'all'
    with open('./CONTROL', 'w') as f:
        f.write(str(TransitionMatrixExample))


